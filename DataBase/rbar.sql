-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-02-2022 a las 03:22:44
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `rbar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id_admin`, `email`, `pass`) VALUES
(1, 'admin@gmail.com', '123456');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `beer`
--

CREATE TABLE `beer` (
  `id_beer` int(11) NOT NULL,
  `name_beer` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `beer`
--

INSERT INTO `beer` (`id_beer`, `name_beer`) VALUES
(1, 'Miller Lite'),
(2, 'Bud Light');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bottle`
--

CREATE TABLE `bottle` (
  `id_bottle` int(11) NOT NULL,
  `name_bottle` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

CREATE TABLE `eventos` (
  `id_evento` int(11) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address1` varchar(100) NOT NULL,
  `address2` varchar(100) NOT NULL,
  `county` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip_code` varchar(100) NOT NULL,
  `guest` varchar(100) NOT NULL,
  `date_event` varchar(100) NOT NULL,
  `hour_ini` varchar(100) NOT NULL,
  `hour_fin` varchar(100) NOT NULL,
  `hour_event` varchar(100) NOT NULL,
  `experience` varchar(100) NOT NULL,
  `wine` varchar(10) NOT NULL,
  `spirits` varchar(100) NOT NULL,
  `food` varchar(10) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `check_admin` varchar(100) NOT NULL,
  `balance` varchar(100) NOT NULL,
  `billing_up` varchar(100) NOT NULL,
  `billing_url` varchar(100) NOT NULL,
  `billing_upa` varchar(100) NOT NULL,
  `billing_urla` varchar(1000) NOT NULL,
  `billing_upb` varchar(100) NOT NULL,
  `billing_urlb` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foods`
--

CREATE TABLE `foods` (
  `id_food` int(11) NOT NULL,
  `name_food` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `foods`
--

INSERT INTO `foods` (`id_food`, `name_food`) VALUES
(1, 'Horseradish-Cheddar Cheese Dip w/Asst Dipping Implements'),
(2, 'Whipped Garlic & Herb Dip w/Asst Dipping Implements'),
(3, 'Hummus & Tapenade w/Mixed Olives, Pita & Assorted Dipping Implements'),
(4, 'Deluxe DipsNThings Tray - All of the Above'),
(5, 'Crudite [mixed raw vegetables] w/Ranch Dip'),
(6, 'Assorted Cheese & Crackers'),
(7, 'Jumbo Shrimp Cocktail'),
(8, 'Crispy-Breaded Cheese Ravioli w/Marinara'),
(9, 'Mini Italian Meatballs w/Marinara'),
(10, 'Boneless Chicken Crispy w/Buffalo & BBQ Sauce'),
(11, 'Boneless Chicken Grilled w/Buffalo & BBQ Sauce'),
(12, 'Wings - Smoked w/Ranch Dressing'),
(13, 'Wings - Honey BBQ w/Ranch Dressing'),
(14, 'Wings - HOT w/Ranch Dressing'),
(15, 'Shrimp Tempura w/Spicy Mayo'),
(16, 'Lump Crab Cakes w/Lemon Aioli');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `guest`
--

CREATE TABLE `guest` (
  `id_guest` int(11) NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  `guest_s` int(11) NOT NULL,
  `guest_u` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `guest`
--

INSERT INTO `guest` (`id_guest`, `min`, `max`, `guest_s`, `guest_u`) VALUES
(1, 1, 50, 3300, 4800),
(2, 51, 100, 5100, 8000),
(3, 101, 150, 6900, 11250),
(4, 151, 200, 8700, 14500),
(5, 201, 250, 10500, 17750),
(6, 251, 300, 12300, 21100),
(7, 301, 350, 15200, 25750),
(8, 351, 400, 17000, 29000),
(9, 401, 450, 18800, 32300),
(10, 451, 500, 20600, 35550),
(11, 501, 550, 22400, 38800),
(12, 551, 600, 24200, 42050),
(13, 601, 650, 26100, 45300),
(14, 651, 700, 28000, 48550),
(15, 701, 750, 29800, 51800),
(16, 751, 800, 31600, 55050),
(17, 801, 850, 33400, 58300),
(18, 851, 900, 35200, 61550),
(19, 901, 950, 37000, 64800),
(22, 951, 1000, 38800, 68050);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `f_name` varchar(100) NOT NULL,
  `l_name` varchar(100) NOT NULL,
  `address_2` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip_code` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `company` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wine`
--

CREATE TABLE `wine` (
  `id_wine` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `purple` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `wine`
--

INSERT INTO `wine` (`id_wine`, `name`, `purple`) VALUES
(1, 'Grey Goose Vodka', 'true'),
(2, 'Pearl Vodka', 'false'),
(3, 'Ford\'s Gin', 'true'),
(4, 'Gordon\'s Gin', 'false'),
(5, 'Cielo Anejo Tequila', 'true'),
(6, 'Exotico Blanco Tequila', 'false'),
(7, 'Belle Meade Reserve Cask Strength Bourbon', 'true'),
(8, 'Buffalo Trace Bourbon', 'true'),
(9, 'Evan Williams Bourbon', 'false'),
(10, 'Corsair Ryemageddon Whiskey', 'true'),
(11, 'Bulleit Rye Whiskey', 'true'),
(12, 'Ezra Brooks Rye Whiskey', 'false'),
(13, 'Davidson Reserve Small Batch TN Whiskey', 'true'),
(14, 'Jack Daniel\'s TN Whiskey', 'false'),
(15, 'Dewar\'s 12 - Year Ancestor Blend Scotch', 'false'),
(16, 'Copper & King\'s American Brandy', 'false'),
(17, 'Campesino Silver X Rum', 'false'),
(18, 'Gosling\'s Black Seal Rum', 'false'),
(19, 'Malibu Coconut Rum', 'false'),
(20, 'Cointreau Orange Liqueur', 'false');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wines`
--

CREATE TABLE `wines` (
  `id_wines` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `wines`
--

INSERT INTO `wines` (`id_wines`, `name`) VALUES
(1, 'Chardonay'),
(2, 'Pinot Grigio'),
(3, 'Sauvignon Blanc'),
(4, 'Cabernet Sauvignon'),
(5, 'Rose'),
(6, 'Pinot Noir'),
(7, 'Merlot');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indices de la tabla `beer`
--
ALTER TABLE `beer`
  ADD PRIMARY KEY (`id_beer`);

--
-- Indices de la tabla `bottle`
--
ALTER TABLE `bottle`
  ADD PRIMARY KEY (`id_bottle`);

--
-- Indices de la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id_evento`);

--
-- Indices de la tabla `foods`
--
ALTER TABLE `foods`
  ADD PRIMARY KEY (`id_food`);

--
-- Indices de la tabla `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`id_guest`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `wine`
--
ALTER TABLE `wine`
  ADD PRIMARY KEY (`id_wine`);

--
-- Indices de la tabla `wines`
--
ALTER TABLE `wines`
  ADD PRIMARY KEY (`id_wines`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `beer`
--
ALTER TABLE `beer`
  MODIFY `id_beer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `bottle`
--
ALTER TABLE `bottle`
  MODIFY `id_bottle` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id_evento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `foods`
--
ALTER TABLE `foods`
  MODIFY `id_food` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `guest`
--
ALTER TABLE `guest`
  MODIFY `id_guest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `wine`
--
ALTER TABLE `wine`
  MODIFY `id_wine` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `wines`
--
ALTER TABLE `wines`
  MODIFY `id_wines` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
