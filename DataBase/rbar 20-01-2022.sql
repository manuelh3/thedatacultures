-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-01-2022 a las 02:22:28
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `rbar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

CREATE TABLE `eventos` (
  `id_evento` int(11) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address1` varchar(100) NOT NULL,
  `address2` varchar(100) NOT NULL,
  `county` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip_code` varchar(100) NOT NULL,
  `guest` varchar(100) NOT NULL,
  `date_event` varchar(100) NOT NULL,
  `hour_event` varchar(100) NOT NULL,
  `experience` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `eventos`
--

INSERT INTO `eventos` (`id_evento`, `email_user`, `name`, `address1`, `address2`, `county`, `city`, `zip_code`, `guest`, `date_event`, `hour_event`, `experience`) VALUES
(4, 'manuel.h.3@hotmail.com', 'cumpleaños de la hija', 'la casona', 'cerca de la laguna', 'huila', 'pitalito', '45651', '3300', '2022-01-17', 'Hour Event: Start 10:56 End  12:00', 0),
(5, 'manuel.h.3@hotmail.com', 'segundo evento de prueba', 'mi cada', 'mi otra casa', 'putumayo', 'la hormiga', '18151', '18800', '2022-01-24', 'Hour Event: Start 00:17 End  14:17', 0),
(6, 'manuel.h.3@hotmail.com', 'tecer evento', 'ay juemadre ', 'en la isla', 'cocora', 'mi corazón', '61265', '29800', '2022-01-29', 'Hour Event: Start 04:19 End  14:19', 0),
(7, 'manuel.h.3@hotmail.com', 'la boda de los rodriguez', 'indiana', 'la casa de pepito', 'florida', 'maimi', '45656', '6900', '2022-02-09', 'Hour Event: Start 07:30 End  09:30', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address_2` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip_code` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `company` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `name`, `address_2`, `city`, `zip_code`, `address`, `phone`, `company`, `email`, `pass`) VALUES
(6, 'Manuel Antonio ', 'Colombia', 'Pitalito', '4100010', 'Cra 1A #24c - 05s', '3166260422', 'thedatacultures', 'manuel.h.3@hotmail.com', '123456'),
(7, 'Jhon Rodriguez', 'Colombia', 'Bogotá', '45657', 'Cra 1A #24c - 05s', '3166260422', 'thedatacultures', 'thedatacultures@gmail.com', '123456'),
(8, 'Erik Lindelof', '', 'Nashville', '37210', '801 B Visco Drive', '615-419-4931', 'rBAR', 'elindelof@rbarapp.com', 'password@1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id_evento`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id_evento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
