<!DOCTYPE html>

<html>

<head>
    <link href="css/estilos.css" type="text/css" rel="stylesheet">
    <meta charset="utf-8">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <script src="https://kit.fontawesome.com/0bddffe200.js" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">

    <script src="js/funciones.js" type="text/jscript"></script>
</head>

<body>

    <?php

    include 'php/conect.php';

    $sql2 = "SELECT * FROM guest";
    $result2 = mysqli_query($con, $sql2);

    $id_guest = "";
    $min = "";
    $max = "";
    $guest = "";

    $html_guest = "";

    while ($fila2  = mysqli_fetch_assoc($result2)) {
        $id_guest = $fila2['id_guest'];
        $min = $fila2['min'];
        $max = $fila2['max'];
        $guest = $fila2['guest_s'];

        $html_guest .= '<option id="' . $guest . '">' . $min . ' - ' . $max . ' ($' . $guest . ' Minimum)</option>';
    }

    ?>

    <div class="content home_user">

        <nav class="info_event">

            <h3 class="title">EVENTS</h3>
            <p>At this time Events are limited to Davidson, Williamson, Rutherford, Sumner, and Wilson Counties. The Bar Minimum is determined by the Guest Count and is inclusive of all taxes, service fees, and gratuities.
                <br><br>
                866-722-7872<br>
                events@rbarapp.com
            </p><br>
            <form action="javascript:NextPage(2)">
                <nav class="data_event">

                    <div class="input_text2">
                        <label>Event Name <span class="requerido">(Required)</span></label>
                        <br>
                        <input type="text" id="nameEvent" style="text-transform: uppercase; background:rgb(241,241,241)" required>
                    </div>
                    <br><br>
                    <div class="input_text2">
                        <label>Address Line 1 <span class="requerido">(Required)</span></label>
                        <br>
                        <input type="text" style="background: rgb(241,241,241);" id="address1" required>
                    </div>
                    <br><br>
                    <div class="input_text2">
                        <label>Address Line 2</label>
                        <br>
                        <input type="text" style="background: rgb(241,241,241);" id="address2">
                    </div>
                    <br><br>

                    <table style="width: 100%">
                        <tr>
                            <td style="padding-right: 10px">
                                <div class="input_text2">
                                    <label>County <span class="requerido">(Required)</span></label>
                                    <br>
                                    <input type="text" style="background: rgb(241,241,241);" id="county" required>
                                </div>
                            </td>

                            <td style="padding-right: 10px">
                                <div class="input_text2">
                                    <label>City <span class="requerido">(Required)</span></label>
                                    <br>
                                    <input type="text" style="background: rgb(241,241,241);" id="city" required>
                                </div>
                            </td>

                            <td>
                                <div class="input_text2">
                                    <label>Zip Code <span class="requerido">(Required)</span></label>
                                    <br>
                                    <input type="text" style="background: rgb(241,241,241);" id="zip_code" maxlength="5" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                                </div>
                            </td>

                        </tr>

                    </table>
                </nav>
                <!-- Hasta aqui -->
                <nav class="info_new">

                    <nav class="left event" style="width: 100% ; margin-top:20px">
                        <b><label>Select # of Guests <span class="requerido">(Required)</span></label></b>

                        <div class="input_text2">
                            <select id="guest">
                                <?php echo $html_guest; ?>
                            </select>
                        </div>
                        <br>
                        <b><label>Pick a Date from the Calendar <span class="requerido">(Required)</span></label></b>
                        <div class="input_text2">
                           
                            <input type="date" value="2015-08-09" id="dateInfo" required min=<?php $hoy = date("Y-m-d");
                                                                            $future = strtotime("+14 day", strtotime($hoy));
                                                                            $future = date("Y-m-d", $future);
                                                                            echo $future; ?>>
                        </div>
                        <br>
                        <b><label>Enter an Event Time Start and End. <span class="requerido">(Required)</span></label></b>
                        <br><br>
                        <div class="input_text2">

                            <table style="width: 100%;">
                                <tr>
                                    <td><label>From:</label>
                                        <br>
                                        <input type="time" list="listaTiempo" id="time1" onchange="Time(this.id)" required />
                                    </td>
                                    <td>
                                        <div class="input_text2">
                                            <label>To:</label>
                                            <br>
                                            <input type="time" list="listaTiempo" onchange="Time(this.id)" id="time2" required>
                                        </div>
                                    </td>
                                </tr>
                            </table>

                            <datalist id="listaTiempo">
                                <option value="08:00"></option>
                                <option value="08:30"></option>
                                <option value="09:00"></option>
                                <option value="09:30"></option>
                                <option value="10:00"></option>
                                <option value="10:30"></option>
                                <option value="11:00"></option>
                                <option value="11:30"></option>
                                <option value="12:00"></option>
                                <option value="12:30"></option>
                                <option value="13:00"></option>
                                <option value="13:30"></option>
                                <option value="14:00"></option>
                                <option value="14:30"></option>
                                <option value="15:00"></option>
                                <option value="15:30"></option>
                                <option value="16:00"></option>
                                <option value="16:30"></option>
                                <option value="17:00"></option>
                                <option value="17:30"></option>
                                <option value="18:00"></option>
                            </datalist>

                        </div>
                        <br><br>
                        <div class="input_text2">
                            <br>
                            <input type="text" id="fullTime" value="Event Time: " readonly>
                        </div>
                    </nav>

                </nav>
                <br>
                <input type="submit" class="btn home_user" value="NEXT">
            </form>
        </nav>


    </div>

    <script>
        window.onload = function() {
            localStorage.setItem('experience', 'Standard');
            localStorage.setItem('wines', 'none');
            localStorage.setItem('spirits', 'Premium');
            localStorage.setItem('food', 'None');
            document.getElementById('nameEvent').value = localStorage.getItem("nameEvent");
            document.getElementById('address1').value = localStorage.getItem("address1");
            document.getElementById('address2').value = localStorage.getItem("address2");
            document.getElementById('county').value = localStorage.getItem("county");
            document.getElementById('city').value = localStorage.getItem("city");
            document.getElementById('zip_code').value = localStorage.getItem("zip_code");

            document.getElementById('guest').value = localStorage.getItem('guest_amount');
            document.getElementById('dateInfo').value = localStorage.getItem('dateEvent');
            document.getElementById('time1').value = localStorage.getItem('ini_hour');
            document.getElementById('time2').value = localStorage.getItem('fin_hour');
            document.getElementById('fullTime').value = localStorage.getItem('hourEvent');

            $('.loader', window.parent.document).fadeOut('fast');
        }
    </script>


</body>



</html>