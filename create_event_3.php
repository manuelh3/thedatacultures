<!DOCTYPE html>

<html>

<head>
    <link href="css/estilos.css" type="text/css" rel="stylesheet">
    <meta charset="utf-8">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <script src="https://kit.fontawesome.com/0bddffe200.js" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/funciones.js" type="text/jscript"></script>
    <script src="js/moment.js" type="text/jscript"></script>
    <script src="https://js.stripe.com/v3/"></script>


</head>

<body>

    <?php
    include 'php/conect.php';

    $sql = "SELECT * FROM wine";
    $result = mysqli_query($con, $sql);

    $sql2 = "SELECT * FROM wines";
    $result2 = mysqli_query($con, $sql2);

    $sql3 = "SELECT * FROM foods";
    $result3 = mysqli_query($con, $sql3);

    $id = "";
    $name = "";
    $purple = "";

    $id_wines = "";
    $name_wines = "";

    $id_food = "";
    $name_food = "";

    $html_spirits = "";
    $html_wines = "";
    $html_food = "";

    while ($fila  = mysqli_fetch_assoc($result)) {
        $id = $fila['id_wine'];
        $name = $fila['name'];
        $purple = $fila['purple'];

        $html_spirits .= '<nav class="box-tag" purple="' . $purple . '" style="margin-left:10px">
        <label>' . $name . '</label>
        <i class="icon-close"></i>
    </nav>';
    }

    while ($fila2  = mysqli_fetch_assoc($result2)) {
        $id_wines = $fila2['id_wines'];
        $name_wines = $fila2['name'];

        $html_wines .= '<nav class="box-tag_d" style="margin-left:20px">
    <table>
        <tr>
            <td><label class="checkbox" style="margin-top: -13px;">
                    <input type="checkbox" id="wine_' . $id_wines . '" onclick="CheckWine(' . $id_wines . ')">
                    <i class="fas fa-check-square" style="margin-left: -18px"></i>
                </label></td>
            <td><label style="margin-left: 40px;">' . $name_wines . '</td>
        </tr>
    </table>
    </nav>';
    }

    while ($fila3  = mysqli_fetch_assoc($result3)) {
        $id_food = $fila3['id_food'];
        $name_food = $fila3['name_food'];

        $html_food .= '<nav class="box-tag_d" style="margin-left: 20px">
        <label>' . $name_food . '</label>
    </nav>';
    }



    ?>

    <div class="content home_user">

        <nav class="info_event">
            <table style="width: 100%">
                <tr>

                    <td style="width: 30%">
                        <h3 class="title" id="nameEvent" style="width: 60%">POOL PARTY</h3>
                    </td>
                    <td style="width: 30%">
                        <p style="margin-left: 50%" id="porcentaje">25%</p>
                        <div class="slide">
                            <nav class="in_slide"></nav>
                        </div>
                    </td>
                    <td style="width: 25%">
                        <p style="float: right; padding-right: 30px;">Total Cost:</p>
                    </td>
                    <td>
                        <div style="width: 100%; float: right" class="input_text2">
                            <input id="totalCost" type="text" style="color: rgb(247,141,40); font-weight: 500; text-align: center" data-type='currency' readonly>
                        </div>
                    </td>
                </tr>


            </table>

            <table class="head-progress">
                <thead>
                    <tr>
                        <th id="page1" active="true">
                            USER EXPERIENCE TYPE
                        </th>
                        <th id="page2" active="false">
                            SPIRITS
                        </th>
                        <th id="page3" active="false">
                            FOOD
                        </th>
                        <th id="page4" active="false">
                            PAYMENT
                        </th>
                    </tr>
                </thead>
            </table>

            <nav class="data_event" style="width: 90%">

                <div id="test1">
                    <nav class="data_event">
                        <br><br>
                        <h3>Enter maximum bar amount:</h3>
                        <input type="text" class="btn" active="false" name="currency-field" id="currency-field" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency" style="text-align: center; float: left;" onchange="Ajuste()">
                    </nav>
                    <nav class="info_new">
                        <br><br>
                        <h3>CHOOSE YOUR EXPERIENCE</h3>

                        <br><br><br><br><br>
                        <label>
                            <label class="checkbox">
                                <input type="checkbox" id="exp1" onchange="Experience(this.id)" checked>
                                <i class="fas fa-check-square"></i>
                            </label>
                            <div style="margin-left: 80px; margin-top: -25px">#1. rBAR Standard Service – Guests use the
                                free rBAR App to order anywhere/anytime during the event. Our Hospitality Tables are
                                setup
                                conveniently throughout the Event Venue, where guests can pick up their drinks from a
                                licensed rBAR server.
                            </div>
                        </label>
                        <br><br><br><br>
                        <label>
                            <label class="checkbox">
                                <input type="checkbox" id="exp2" onchange="Experience(this.id)">
                                <i class="fas fa-check-square"></i>
                            </label>
                            <div style="margin-left: 80px; margin-top: -25px">#2. Ultimate Service Experience – Guests
                                do
                                not order from the rBAR App. rBAR staff are positioned throughout the event with Tablets
                                to
                                take guest drink orders without any lines. Our Hospitality Tables are setup conveniently
                                throughout the Event Venue, where guests can pick up their drinks from a licensed rBAR
                                server. This service increases the required Bar Minimum.</div>
                        </label>

                    </nav>
                    <!-- <div class="nota">
                        <p><b>NOTE:</b> Enter maximum bar amount. All ordering will end when amount is reached (Host
                            will be notified when nearing limit) unless the limit is increased or the bar is kept open
                            as a cash bar. A 20% gratuity will be added to your final total.</p>
                    </div> -->
                    <br><br>
                    <input type="button" style="width: 20%" class="btn home_user" value="NEXT" onclick="NextOption(2)">
                    <input type="button" style="width: 20%; margin-right: 20px;" class="btn home_user" value="PREV" onclick="NextOption(1)">
                </div>

                <div id="test2" style="display: none">
                    <table class="head-progress second">
                        <thead>
                            <tr>
                                <th active="true">
                                    SPIRITS
                                </th>
                                <th active="false">
                                    WINE
                                </th>
                                <th active="false">
                                    DRAFT BEER
                                </th>
                                <th active="false">
                                    CAN & BOTTLE, CIDER & SELTZER
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <br><br><br><br>
                    <div class="nota" style="height: 30px; vertical-align: middle;">
                        <label>
                            <br>
                            <label class="checkbox">
                                <input type="checkbox" id="licores" onchange="CheckChange(this.id)">
                                <i class="fas fa-check-square"></i>
                            </label>
                            <div style="margin-left: 50px; margin-top: -15px">Check box if you want to exclude premium
                                liquors from your event selections.</div>
                        </label>
                    </div>
                    <br><br><br>
                    <div class="user_more_info" style="background-color: white; border:none; overflow-y:scroll">
                        <section class="items_wine" style="padding: 20px;">
                            <?php echo $html_spirits; ?>
                        </section>
                    </div>

                    <br><br>
                    <input type="button" style="width: 20%" class="btn home_user" value="NEXT" onclick="NextOption(3)">
                    <input type="button" style="width: 20%; margin-right: 20px;" class="btn home_user" value="PREV" onclick="PrevOption(1)">
                </div>

                <div id="test3" style="display: none">
                    <table class="head-progress second">
                        <thead>
                            <tr>
                                <th active="true">
                                    SPIRITS
                                </th>
                                <th active="true">
                                    WINE
                                </th>
                                <th active="false">
                                    DRAFT BEER
                                </th>

                                <th active="false">
                                    CAN & BOTTLE, CIDER & SELTZER
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <br><br><br><br>
                    <div class="nota">
                        <p><b>Note:</b> Sparkling Wine is always included.</p>
                    </div>
                    <br><br><br>
                    <h3>SELECT UP TO FOUR</h3>
                    <section class="items_wine" style="padding: 20px;">
                        <?php echo $html_wines; ?>
                    </section>

                    <br><br><br><br><br><br><br><br><br><br>
                    <input type="button" style="width: 20%" class="btn home_user" value="NEXT" onclick="NextOption(4)">
                    <input type="button" style="width: 20%; margin-right: 20px;" class="btn home_user" value="PREV" onclick="PrevOption(2)">
                </div>

                <div id="test4" style="display: none">
                    <table class="head-progress second">
                        <thead>
                            <tr>
                                <th active="true">
                                    SPIRITS
                                </th>
                                <th active="true">
                                    WINE
                                </th>
                                <th active="true">
                                    DRAFT BEER
                                </th>

                                <th active="false">
                                    CAN & BOTTLE, CIDER & SELTZER
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <br><br><br><br>
                    <div class="nota">
                        <p><b>Draft beer:</b> Always Included</p>
                    </div>
                    <br><br><br>
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <nav class="box-tag_d">
                                    <label>Miller Lite</label>

                                </nav>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                        </tr>

                        <tr>
                            <td>
                                <nav class="box-tag_d">
                                    <label>Bud Light</label>

                                </nav>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                    <br><br><br><br><br><br><br><br><br><br>
                    <input type="button" style="width: 20%" class="btn home_user" value="NEXT" onclick="NextOption(5)">
                    <input type="button" style="width: 20%; margin-right: 20px;" class="btn home_user" value="PREV" onclick="PrevOption(3)">
                </div>

                <div id="test5" style="display: none">
                    <table class="head-progress second">
                        <thead>
                            <tr>
                                <th active="true">
                                    SPIRITS
                                </th>
                                <th active="true">
                                    WINE
                                </th>
                                <th active="true">
                                    DRAFT BEER
                                </th>

                                <th active="true">
                                    CAN & BOTTLE, CIDER & SELTZER
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <br><br><br><br>
                    <div class="nota">
                        <p><b>Draft beer:</b> Always Included</p>
                    </div>
                    <br><br><br>
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <nav class="box-tag_d">
                                    <label>Budweiser Can</label>

                                </nav>
                            </td>
                            <td>
                                <nav class="box-tag_d">
                                    <label>Pinot Grigio</label>

                                </nav>
                            </td>
                            <td>
                                <nav class="box-tag_d">
                                    <label>Budweiser Bottle</label>

                                </nav>
                            </td>
                            <td>
                                <nav class="box-tag_d">
                                    <label>Cabernet Sauvignon</label>

                                </nav>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <nav class="box-tag_d">
                                    <label>Rose</label>

                                </nav>
                            </td>
                            <td>
                                <nav class="box-tag_d">
                                    <label>Pinot Noirt</label>

                                </nav>
                            </td>
                            <td>
                                <nav class="box-tag_d">
                                    <label>Merlot</label>

                                </nav>
                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                    <br><br><br><br><br><br><br><br><br><br>
                    <input type="button" style="width: 20%" class="btn home_user" value="NEXT" onclick="NextOption(6)">
                    <input type="button" style="width: 20%; margin-right: 20px;" class="btn home_user" value="PREV" onclick="PrevOption(4)">
                </div>

                <div id="test6" style="display: none">

                    <br>
                    <div class="nota" style="height: 30px;">
                        <label style="height: 120px; margin-top: 5px;">
                            <br>
                            <label class="checkbox">
                                <input type="checkbox" id="foods" onchange="CheckChange(this.id)">
                                <i class="fas fa-check-square"></i>
                            </label>
                            <div style="margin-left: 50px; margin-top: -15px"><b>Food:</b> Check here if you need food for your event</div>
                        </label>

                    </div>
                    <br><br><br>
                    <section id="table-box" style="display: none;">
                        <section class="items_wine" style="padding: 20px;">
                            <?php echo $html_food; ?>
                        </section>
                    </section>
                    <br><br>
                    <input type="button" style="width: 20%" class="btn home_user" value="NEXT" onclick="NextOption(7)">
                    <input type="button" style="width: 20%; margin-right: 20px;" class="btn home_user" value="PREV" onclick="PrevOption(5)">
                </div>

                <div id="test7" style="display: none">

                    <br><br>
                    <div class="nota">
                        <p><b>PAYMENT: You must pay 50% to create the event</b></p>
                    </div>
                    <br><br><br>
                    <h3>Event: <span id="n_event"></span></h3>
                    <br>
                    <h3>Date: <span id="d_event"></span></h3>
                    <br>
                    <h3>Event time: <span id="t_event"></span></h3>
                    <br>
                    <h3>Full payment: <span id="f_event"></span></h3>
                    <br>
                    <h3>Payment 50%: <span id="p_event"></span></h3>
                    <script>
                        var guest_mid = parseInt(localStorage.getItem('guest'));
                        var guest_t = guest_mid / 2;

                        var year =  localStorage.getItem('dateEvent').slice(0,4);
                        var mont = localStorage.getItem('dateEvent').slice(5,7);
                        var day = localStorage.getItem('dateEvent').slice(8,10);

                        document.getElementById('n_event').innerHTML = localStorage.getItem('nameEvent');
                        document.getElementById('d_event').innerHTML = mont +'/'+ day +'/'+ year;
                        document.getElementById('t_event').innerHTML = localStorage.getItem('hourEvent');
                        document.getElementById('f_event').innerHTML = "$ " + guest_mid.toLocaleString('en-US') + ".00";
                        document.getElementById('p_event').innerHTML = "$ " + guest_t.toLocaleString('en-US') + ".00 ";
                    </script>
                    <!-- <div class="card_pay">
                        <i class="icon-more" style="float: right; padding: 15px; font-size: 20px"></i>
                        <nav>
                            <span class="icon-maestro" style="font-size: 45px"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span></span>

                            <br><br><br>
                            <h2 style="font-weight: 400">MasterCard ... 4159</h2><br>
                            <h4 style="font-weight: 100">Vence 12/2026</h4>
                            <h4 style="font-weight: 100; margin-top: -15px">Street 72 - 28, OHIO</h4>
                        </nav>
                        <br><br><br><br>
                    </div> -->

                    <!-- <div class="card_pay more">
                        <nav>
                            <h2 style="font-weight: 400"> Add payment method</h2>
                        </nav>
                        <br><br><br><br>
                    </div> -->
                    <br><br><br><br><br>
                    <br><br>
                    <input type="button" style="width: 20%" class="btn home_user" value="PAYMENT" onclick="Payment('activo')">
                    <input type="button" style="width: 20%; margin-right: 20px;" class="btn home_user" value="PREV" onclick="PrevOption(6)">
                </div>
            </nav>

        </nav>


    </div>

    <script>
        function Experience(idCheck) {

            var itemCheck1 = document.getElementById('exp1').checked;
            var itemCheck2 = document.getElementById('exp2').checked;

            switch (idCheck) {
                case 'exp1':
                    document.getElementById('exp1').checked = true;
                    document.getElementById('exp2').checked = false;
                    localStorage.setItem('experience', 'Standard');
                    if (itemCheck1 != false) {
                        GetGuest('guest_s');
                    }
                    break;

                case 'exp2':
                    document.getElementById('exp1').checked = false;
                    document.getElementById('exp2').checked = true;
                    localStorage.setItem('experience', 'Ultimate');
                    if (itemCheck2 != false) {
                        GetGuest('guest_u');
                    }

                    break;
            }

        }

        var cost = 0;

        function GetGuest(experience) {
            var obj_experience = JSON.stringify(experience);
            var obj_guest = JSON.stringify(localStorage.getItem('guest'));

            $.ajax({
                cache: false,
                type: 'POST',
                url: 'php/get_guest.php',
                data: {
                    obj_experience: obj_experience,
                    obj_guest: obj_guest
                },
                success: function(res) {
                    localStorage.setItem('guest', res);
                    document.getElementById('totalCost').value = '$' + parseInt(localStorage.getItem('guest')).toLocaleString("es-Us");
                    document.getElementById('currency-field').value = '$' + parseInt(localStorage.getItem('guest')).toLocaleString("es-Us");
                }
            });
        }

        function Ajuste() {
            var totalCost = document.getElementById('totalCost').value.substring(1);
            var ini = document.getElementById('currency-field').value.substring(0, 1);
            var currency = 0;

            if (ini != "$") {
                currency = document.getElementById('currency-field').value;
            } else {
                currency = document.getElementById('currency-field').value.substring(1);
            }

            if (parseInt(currency) < parseInt(totalCost)) {
                parent.PopAlert("The value must be greater than $" + totalCost);
                document.getElementById('currency-field').value = "$" + localStorage.getItem('guest');
            } else {
                document.getElementById('currency-field').value = "$" + currency;
            }
        }

        window.onload = function() {
            cost = parseInt(localStorage.getItem('guest'));
            document.getElementById('totalCost').value = "$" + cost.toLocaleString("en-Us");
            document.getElementById('currency-field').value = "$" + cost.toLocaleString("en-Us");
            document.getElementById('nameEvent').innerHTML = localStorage.getItem('nameEvent').toUpperCase();
        }

        function PrevOption(idPage) {
            switch (idPage) {

                case 1:
                    $("#test1").slideDown(0);
                    $("#test2").slideUp(0);

                    $("#page2").attr('active', 'false');
                    $("#page1").attr('active', 'true');
                    $(".in_slide").css('width', '25%');
                    document.getElementById('porcentaje').innerHTML = '25%';
                    break;

                case 2:
                    $("#test3").slideUp(0);
                    $("#test2").slideDown(0);

                    $("#page2").attr('active', 'true');
                    $("#page3").attr('active', 'false');
                    $("#page1").attr('active', 'true');
                    $(".in_slide").css('width', '50%');
                    document.getElementById('porcentaje').innerHTML = '50%';
                    break;

                case 3:
                    $("#test4").slideUp(0);
                    $("#test3").slideDown(0);

                    $("#page2").attr('active', 'true');
                    //$("#page3").attr('active', 'true');
                    $("#page1").attr('active', 'true');
                    $(".in_slide").css('width', '75%');
                    document.getElementById('porcentaje').innerHTML = '75%';
                    break;

                case 4:
                    $("#test5").slideUp(0);
                    $("#test4").slideDown(0);

                    $("#page2").attr('active', 'true');
                    //$("#page3").attr('active', 'true');
                    $("#page1").attr('active', 'true');
                    $(".in_slide").css('width', '75%');
                    document.getElementById('porcentaje').innerHTML = '75%';
                    break;

                case 5:
                    $("#test6").slideUp(0);
                    $("#test5").slideDown(0);

                    $("#page2").attr('active', 'true');
                    $("#page3").attr('active', 'false');
                    $("#page1").attr('active', 'true');
                    $(".in_slide").css('width', '75%');
                    document.getElementById('porcentaje').innerHTML = '75%';
                    break;

                case 6:
                    $("#test7").slideUp(0);
                    $("#test6").slideDown(0);

                    $("#page2").attr('active', 'true');
                    $("#page3").attr('active', 'true');
                    $("#page1").attr('active', 'true');
                    $("#page4").attr('active', 'false');
                    $(".in_slide").css('width', '75%');
                    document.getElementById('porcentaje').innerHTML = '75%';
                    break;

            }
        }

        function NextOption(idPage) {

            switch (idPage) {

                case 1:
                    window.open("create_event.php", "_self");
                    break;

                case 2:
                    $("#test1").slideUp('fast');
                    $("#test2").slideDown('fast');

                    $("#page2").attr('active', 'true');
                    $(".in_slide").css('width', '50%');
                    document.getElementById('porcentaje').innerHTML = '50%';
                    break;

                case 3:
                    $("#test2").slideUp('fast');
                    $("#test3").slideDown('fast');

                    //$("#page3").attr('active', 'true');
                    $(".in_slide").css('width', '75%');
                    document.getElementById('porcentaje').innerHTML = '75%';
                    break;

                case 4:
                    localStorage.setItem("wines", wines.toString());
                    $("#test3").fadeOut(0);
                    $("#test4").fadeIn(0);
                    break;

                case 5:
                    $("#test4").fadeOut(0);
                    $("#test5").fadeIn(0);
                    break;

                case 6:
                    $("#page3").attr('active', 'true');
                    $("#test5").fadeOut(0);
                    $("#test6").fadeIn(0);
                    break;

                case 7:
                    $("#test6").fadeOut(0);
                    $("#test7").fadeIn(0);

                    $("#page4").attr('active', 'true');
                    $(".in_slide").css('width', '100%');
                    document.getElementById('porcentaje').innerHTML = '100%';
                    break;


            }

        }

        var cont_wines = 0;
        var wines = [];
        var flag_wines = true;

        function CheckWine(id_wine) {

            if (document.getElementById('wine_' + id_wine).checked == false) {
                document.getElementById('wine_' + id_wine).checked = false;
                cont_wines--;
                for (i = 0; i < 4; i++) {
                    if (wines[i] == id_wine) {
                        wines.splice(i, 1);
                    }
                }
            } else {
                if (cont_wines < 4) {
                    wines.push(id_wine);
                    cont_wines++;
                } else {
                    window.parent.PopAlert('Pick only four');
                    document.getElementById('wine_' + id_wine).checked = false;
                }
            }

        }
    </script>


</body>



</html>