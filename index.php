<!DOCTYPE html>

<html>

<head>
    <link href="css/estilos.css" type="text/css" rel="stylesheet">
    <meta charset="utf-8">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <script src="https://kit.fontawesome.com/0bddffe200.js" crossorigin="anonymous"></script>

    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="js/funciones.js" type="text/javascript"></script>

</head>

<body>

    <?php
    session_start();
    
    ?>

    <div class="sombra">
        <div class="alert">
            <nav>
                <i class="icon-alert"></i><br><br><br>
                <h2 id="msm_alert">Please must accept the policies</h2><br><br><br>
                <input type="button" id="btn_alert" onclick="CloseAlert()" class="btn" value="OK">
            </nav>
        </div>

    </div>

    <div id="content">
        <nav class="left">
            <nav class="info_login">
                <h1>Sign In</h1>
                <br><br><br><br><br><br>
                <form action="javascript:LogIn()">
                    <div class="input_text">
                        <label>Email Adress</label>
                        <i class="icon-Email2"></i>
                        <input id="email" class="text" type="email" value="<?php if(isset($_SESSION['email'])){echo $_SESSION['email'];} ?>" required>
                    </div>
                    <br><br><br><br>
                    <div class="input_text">
                        <label>Password</label>
                        <i class="icon-Password"></i>
                        <input id="pass" class="text" type="password" value="<?php if(isset($_SESSION['pass'])){echo $_SESSION['pass'];} ?>" required>
                    </div>
                    <br><br><br>
                    <input type="submit" class="btn" value="CONTINUE" onclick="LogIn()">
                </form>
                <br><br><br>
                <a href="create.html">Create Account</a>
                <a class="live_rigth">Forgot password?</a>

            </nav>

        </nav>
        <nav class="rigth">
            <center>
                <img class="logo" src="img/logo_2.png">
            </center>
        </nav>
    </div>
    <script>
    
        function LogIn() {
            
            var email = document.getElementById('email').value;
            var pass = document.getElementById('pass').value;
            
            if(email == "" || pass == ""){
                if(email == ""){
                    PopAlert('Please Enter Your Email');
                }

                if(pass == ""){
                    PopAlert('Please Enter Your Password');
                }
               
               }
            else
                {
                    var obj_email = JSON.stringify(email);
                    var obj_pass = JSON.stringify(pass);

            $.ajax({
                cache: false,
                method: 'post',
                url: 'php/login.php',
                data: {
                    obj_email: obj_email,
                    obj_pass: obj_pass
                },
                success: function(res) {
                    if(res == 'error'){
                       PopAlert('Incorrect Email Address o Incorrect Password');
                       }
                    else{
                
                        window.open('master.php','_self');
                    }
                        
                }
            });
                }
        }
        
        //window.history.pushState({},'','/test_rbar');
    </script>


</body>



</html>