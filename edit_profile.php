<!DOCTYPE html>

<html>

<head>
    <link href="css/estilos.css" type="text/css" rel="stylesheet">
    <meta charset="utf-8">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <script src="https://kit.fontawesome.com/0bddffe200.js" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>

    <!--Librerias de FullCalendar-->
    <script src="fullcalendar/lib/moment.min.js"></script>
    <link rel="stylesheet" href="fullcalendar/fullcalendar.min.css">
    <script src="fullcalendar/fullcalendar.min.js"></script>

    <!---->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/funciones.js" type="text/jscript"></script>


</head>

<body>

    <?php
    session_start();

    include 'php/conect.php';

    $email = $_SESSION['email'];

    $sql = "SELECT * FROM usuarios WHERE email ='$email'";
    $result = mysqli_query($con, $sql);

    $id_user = "";
    $f_name = "";
    $l_name = "";
    $telefono = "";
    $zip_code = "";
    $county = "";
    $company = "";
    $address1 = "";
    $address2 = "";

    while ($fila  = mysqli_fetch_assoc($result)) {
        $id_user = $fila['id_usuario'];
        $f_name = $fila['f_name'];
        $l_name = $fila['l_name'];
        $telefono = $fila['phone'];
        $zip_code = $fila['zip_code'];
        $county = $fila['city'];
        $company = $fila['company'];
        $address1 = $fila['address'];
        $address2 = $fila['address_2'];
    }

    ?>

    <div class="content home_user">

        <nav class="rigth home_user" style="width: 100%;">

            <section class="programa">
                <br><br>
                <h2>EDIT MY PROFILE</h2>
                <br><br>

                <div class="camp_text" style="height: 45vh">
                    <table class="profile">
                        <tr>
                            <td><b>First Name</td>
                            <td><b>Telephone</td>
                            <td><b>Zip Code</td>
                            <td><b>County</td>
                        </tr>
                        <tr>
                            <td><input type="text" class="input_text input" style="height:35px; font-family:nova; margin-top:-20px; width:90%;font-size:18px; padding-left:20px" id="fname" value="<?php echo $f_name; ?>"></td>
                            <td><input type="text" class="input_text input" style="height:35px; font-family:nova; margin-top:-20px; width:90%;font-size:18px; padding-left:20px" id="phone" value="<?php echo $telefono; ?>"></td>
                            <td><input type="text" class="input_text input" style="height:35px; font-family:nova; margin-top:-20px; width:90%;font-size:18px; padding-left:20px" id="zip_code" value="<?php echo $zip_code; ?>"></td>
                            <td><input type="text" class="input_text input" style="height:35px; font-family:nova; margin-top:-20px; width:90%;font-size:18px; padding-left:20px" id="county" value="<?php echo $county; ?>"></td>
                        </tr>
                        <tr>
                            <td><b>Last Name</td>
                            <td><b>Company</td>
                            <td><b>Address Line 1</td>
                            <td><b>Address Line 2</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="input_text input" style="height:35px; font-family:nova; margin-top:-20px; width:90%;font-size:18px; padding-left:20px" id="lname" value="<?php echo $l_name; ?>"></td>
                            <td><input type="text" class="input_text input" style="height:35px; font-family:nova; margin-top:-20px; width:90%;font-size:18px; padding-left:20px" id="company" value="<?php echo $company; ?>"></td>
                            <td><input type="text" class="input_text input" style="height:35px; font-family:nova; margin-top:-20px; width:90%;font-size:18px; padding-left:20px" id="address" value="<?php echo $address1; ?>"></td>
                            <td><input type="text" class="input_text input" style="height:35px; font-family:nova; margin-top:-20px; width:90%;font-size:18px; padding-left:20px" id="address_2" value="<?php echo $address2; ?>"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><b>Email</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="input_text input" style="height:35px; font-family:nova; margin-top:-20px; width:90%;font-size:18px; padding-left:20px" id="email" value="<?php echo $_SESSION['email']; ?>"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                    </table>

                </div>
                <br><br><br>
                <table>
                    <tr>
                        <td style="width:300px">
                            <div class="camp_text" style="height: 20vh; width:95%; text-align:center; cursor:pointer" onclick="UpdateUser(<?php echo $id_user; ?>)">
                                <br><br>
                                <i class="icon-user-4" style="font-size: 45px;"></i>
                                <br><br>
                                <h3>Save Changes</h3>
                            </div>
                        </td>
                    </tr>
                </table>


            </section>

        </nav>

    </div>

    <script>
        function Next() {
            window.open('create_event.html', '_self');
            $('#drink', window.parent.document).attr('active', 'true');
            $('#home', window.parent.document).attr('active', 'false');
        }

        function ChangeSection(btn) {
            switch (btn) {
                case 'btn_calendar':
                    $('#btn_calendar').attr('state', 'enabled');
                    $('#btn_list').attr('state', '');
                    $('.calendarios').fadeIn(0);
                    $('.listas').fadeOut(0);
                    break;

                case 'btn_list':
                    $('#btn_list').attr('state', 'enabled');
                    $('#btn_calendar').attr('state', '');
                    $('.calendarios').fadeOut(0);
                    $('.listas').fadeIn(0);
                    break;

            }
        }

        function UpdateUser(id) {
            var obj_id = JSON.stringify(id);
            var obj_f_name = JSON.stringify(document.getElementById('fname').value);
            var obj_l_name = JSON.stringify(document.getElementById('lname').value);
            var obj_city = JSON.stringify(document.getElementById('county').value);
            var obj_zipCode = JSON.stringify(document.getElementById('zip_code').value);
            var obj_address = JSON.stringify(document.getElementById('address').value);
            var obj_address_2 = JSON.stringify(document.getElementById('address_2').value);
            var obj_phone = JSON.stringify(document.getElementById('phone').value);
            var obj_company = JSON.stringify(document.getElementById('company').value);
            var obj_email = JSON.stringify(document.getElementById('email').value);

            $.ajax({
                cache: false,
                type: 'POST',
                url: 'php/update_user.php',
                data: {
                    obj_id: obj_id,
                    obj_f_name: obj_f_name,
                    obj_l_name:obj_l_name,
                    obj_city:obj_city,
                    obj_zipCode:obj_zipCode,
                    obj_address:obj_address,
                    obj_address_2:obj_address_2,
                    obj_phone:obj_phone,
                    obj_company:obj_company,
                    obj_email:obj_email
                },
                success: function(res) {
                    if(res == 'ok'){
                        window.parent.PopAlert("Updated");
                        location.reload();
                    }
                    else{
                        window.parent.PopAlert("Error: " + res);
                    }
                }
            });
        }
    </script>

</body>



</html>