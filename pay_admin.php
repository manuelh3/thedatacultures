<!DOCTYPE html>

<html>

<head>
    <link href="css/estilos.css" type="text/css" rel="stylesheet">
    <meta charset="utf-8">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <script src="https://kit.fontawesome.com/0bddffe200.js" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>

    <!--Librerias de FullCalendar-->
    <script src="fullcalendar/lib/moment.min.js"></script>
    <link rel="stylesheet" href="fullcalendar/fullcalendar.min.css">
    <script src="fullcalendar/fullcalendar.min.js"></script>

    <!---->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style2.css">
    <script src="js/funciones.js" type="text/jscript"></script>


</head>

<body>

    <?php

    include 'php/conect.php';

    $sql = "SELECT * FROM eventos WHERE estado='activo'";
    $result = mysqli_query($con, $sql);

    $id_event = "";
    $correo = "";
    $name = "";
    $date = "";
    $city = "";
    $cost = "";
    $balance = "";
    $file_up = "";
    $file_upa = "";
    $file_upb = "";

    $ver1 = "";
    $ver2 = "";
    $ver3 = "";

    $color = "";
    $color_file = "";
    $text = "";
    $cont = 0;

    $html_list = "";


    while ($fila  = mysqli_fetch_assoc($result)) {

        $id_event = $fila['id_evento'];
        $correo = $fila['email_user'];
        $name = $fila['name'];
        $date = $fila['date_event'];
        $city = $fila['city'];
        $cost = $fila['guest'];
        $balance = $fila['balance'];
        $file_up = $fila['billing_up'];
        $file_upa = $fila['billing_upa'];
        $file_upb = $fila['billing_upb'];

        switch ($balance) {
            case 'subscription':
                $color = "rgb(247,141,40)";
                $text = 'Partial Payment';
                break;

            case 'paid':
                $color = "rgb(139,197,129)";
                $text = 'Paid Full';
                break;

            case 'unpaid':
                $color = "rgb(235,83,84)";
                $text = 'Unpaid';
                break;
        }

        if($file_up == "false")$ver1 = 'style="display:none"';
        if($file_upa == "false")$ver2 = 'style="display:none"';
        if($file_upb == "false")$ver3 = 'style="display:none"';

        if ($file_up == 'true') $color_file = 'rgba(247,141,40,1)';
        else $color_file = 'rgba(0,0,0,0.4)';

        $cont++;

        $date_ini = date_create($date);
        $date_set = date_format($date_ini, "m/d/Y");

        $html_list .= '<tr>
        <td>' . $correo . '</td>
        <td>' . $name . '</td>
        <td>' . $date_set . '</td>
        <td>' . $city . '</td>
        <td>$ ' . number_format($cost) . '.00</td>
        <td>
            <div class="biling_state" id="balance' . $cont . '" onclick="Balance(this.id)" style="background-color:' . $color . '">
                <p>' . $text . '</p>
            </div>
            <nav class="menu_balance" name="balance' . $cont . '">
                <br>
                <table>
                    <tr>
                        <td><b>Balance</b></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="paid" id="paid" onclick="UpdateBalance(this.id,' . $id_event . ')">Paid Full</div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="sub" id="subscription" onclick="UpdateBalance(this.id,' . $id_event . ')">Partial Payment</div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="unpaid" id="unpaid" onclick="UpdateBalance(this.id,' . $id_event . ')">Unpaid</div>
                        </td>
                    </tr>

                </table>
            </nav>
        </td>
        <td>
            
            <i class="icon-invoice-1" style="font-size: 30px; cursor:pointer; color:' . $color_file . '" id="billing' . $cont . '" onclick="Billing(this.id)"></i>
                
            <nav class="menu_billing" name="billing' . $cont . '">
                <br>
                <table>
                    <tr>
                        <td><b>Billing</b></td>
                    </tr>
                    <tr>
                        <td>
                        <div class="image-upload">
                        <label for="file-input' . $id_event . '" style="cursor:pointer">
                            <div class="paid">Upload Invoice 1</div>
                        </label>
                        <input id="file-input' . $id_event . '" onchange="UpFile(' . $id_event . ',1)" type="file">
                        </div>
                        <a href="facturas/factura1_'.$id_event.'.pdf" '.$ver1.' download><i class="icon-Descargar" style="float:right; margin-top:-33px; font-weight: 900; margin-right:5px"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <div class="image-upload">
                        <label for="file-inputa' . $id_event . '" style="cursor:pointer">
                            <div class="paid">Upload Invoice 2</div>
                        </label>
                        <input id="file-inputa' . $id_event . '" onchange="UpFile(' . $id_event . ',2)" type="file">
                        </div>
                        <a href="facturas/factura2_'.$id_event.'.pdf" '.$ver2.' download><i class="icon-Descargar" style="float:right; margin-top:-33px; font-weight: 900; margin-right:5px"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <div class="image-upload">
                        <label for="file-inputb' . $id_event . '" style="cursor:pointer">
                            <div class="paid">Upload Invoice 3</div>
                        </label>
                        <input id="file-inputb' . $id_event . '" onchange="UpFile(' . $id_event . ',3)" type="file">
                        </div>
                        <a href="facturas/factura3_'.$id_event.'.pdf" '.$ver3.' download><i class="icon-Descargar" style="float:right; margin-top:-33px; font-weight: 900; margin-right:5px"></i></a>
                        </td>
                    </tr>
                </table>
            </nav>
        </td>
    </tr>';
    }

    ?>

    <div class="content home_user">

        <nav class="rigth home_user" style="width: 100%;">

            <section class="programa">
                <br><br><br>
                <section class="listas">
                    <div class="calendario_2">
                        <div id="inside_calendario_2">
                            <table class="table1 title">
                                <thead>
                                    <tr>
                                        <th>CLIENT</th>
                                        <th>EVENT NAME</th>
                                        <th>EVENT DATE</th>
                                        <th>CITY</th>
                                        <th>COST</th>
                                        <th>BALANCE</th>
                                        <th>UPLOAD INVOICE</th>
                                    </tr>
                                </thead>
                            </table>
                            <section class="contenido_listas">
                                <table class="table_listas">
                                    <tbody>
                                        <?php echo $html_list; ?>
                                    </tbody>
                                </table>
                            </section>
                        </div>
                    </div>
                </section>

            </section>

        </nav>

    </div>

    <script>
        window.onload = function() {
            $('.loader', window.parent.document).fadeOut('fast');
        }

        function Next() {
            window.open('create_event.html', '_self');
            $('#drink', window.parent.document).attr('active', 'true');
            $('#home', window.parent.document).attr('active', 'false');
        }

        function ChangeSection(btn) {
            switch (btn) {
                case 'btn_calendar':
                    $('#btn_calendar').attr('state', 'enabled');
                    $('#btn_list').attr('state', '');
                    $('.calendarios').fadeIn(0);
                    $('.listas').fadeOut(0);
                    break;

                case 'btn_list':
                    $('#btn_list').attr('state', 'enabled');
                    $('#btn_calendar').attr('state', '');
                    $('.calendarios').fadeOut(0);
                    $('.listas').fadeIn(0);
                    break;

            }
        }

        var n_balance = "";

        function Balance(id_balance) {
            $('.menu_balance[name=' + id_balance + ']').slideToggle('fast');
            n_balance = id_balance;
        }

        function UpdateBalance(id_option, id_event) {

            var obj_function = JSON.stringify('UpdateBalance');
            var obj_event = JSON.stringify(id_event);
            var obj_state = JSON.stringify(id_option);

            $.ajax({
                cache: false,
                method: 'post',
                url: 'php/funciones_php.php',
                data: {
                    obj_function: obj_function,
                    obj_event: obj_event,
                    obj_state: obj_state
                },
                success: function() {
                    location.reload();
                }
            });

        }

        function Billing(id_billing) {
            $('.menu_billing[name=' + id_billing + ']').slideToggle('fast');
        }

        function UpFile(id_event, factura) {

            var fileData = "";
            var data = new FormData();

            switch (factura) {
                case 1:
                    fileData = $('#file-input' + id_event).prop('files')[0];
                    data.append('filename', 'factura1_' + id_event);
                    data.append('factura', 'factura1');
                    break;

                case 2:
                    fileData = $('#file-inputa' + id_event).prop('files')[0];
                    data.append('filename', 'factura2_' + id_event);
                    data.append('factura', 'factura2');
                    break;

                case 3:
                    fileData = $('#file-inputb' + id_event).prop('files')[0];
                    data.append('filename', 'factura3_' + id_event);
                    data.append('factura', 'factura3');
                    break;
            }

            data.append('file', fileData);
            data.append('event', id_event);

            $.ajax({
                type: 'POST',
                url: 'php/up_factura.php',
                contentType: false,
                processData: false,
                data: data,
                success: function(res) {

                    if (res == 'ok') {
                        window.parent.PopAlert('Invoice uploaded successfully');
                        location.reload();
                    }
                }
            });
        }
    </script>

</body>



</html>