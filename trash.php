<!DOCTYPE html>

<html>

<head>
    <link href="css/estilos.css" type="text/css" rel="stylesheet">
    <meta charset="utf-8">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <script src="https://kit.fontawesome.com/0bddffe200.js" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>

    <!--Librerias de FullCalendar-->
    <script src="fullcalendar/lib/moment.min.js"></script>
    <link rel="stylesheet" href="fullcalendar/fullcalendar.min.css">
    <script src="fullcalendar/fullcalendar.min.js"></script>

    <!---->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/funciones.js" type="text/jscript"></script>


</head>

<body>

    <?php
    session_start();

    include 'php/conect.php';

    $email = $_SESSION['email'];

    $sql = "SELECT * FROM eventos WHERE email_user ='$email'";
    $result = mysqli_query($con, $sql);

    $id = "";
    $name = "";
    $date = "";
    $hour = "";
    $address = "";
    $estado = "";

    $html_list = "";

    while ($fila  = mysqli_fetch_assoc($result)) {
        $id = $fila['id_evento'];
        $name = $fila['name'];
        $date = $fila['date_event'];
        $hour = $fila['hour_event'];
        $address = $fila['address1'];
        $estado = $fila['estado'];

        $page = 'drink';

        $date_ini = date_create($date);
        $date_set = date_format($date_ini,"m/d/Y");

        if ($estado == 'archivado') {
            $html_list .= '<tr>
        <td>' . $name . '</td>
        <td>' . $date_set . '</td>
        <td>' . $hour . '</td>
        <td>' . $address . '</td>
        <td><i class="icon-credit-card" id="' . $id . '" onclick="Reload(this.id)" style="font-size: 30px; cursor:pointer; color: rgb(0,255,117)"></i></td>
        </tr>';
        }
    }

    ?>

    <div class="content home_user">

        <nav class="rigth home_user" style="width: 100%;">

            <section class="programa">
                <br><br>
                <h2>ARCHIVED EVENTS</h2>
                <br><br>

                <section class="listas">
                    <div class="calendario_2">
                        <div id="inside_calendario_2">
                            <table class="table1 title">
                                <thead>
                                    <tr>
                                        <th>EVENT NAME</th>
                                        <th>EVENT DATE</th>
                                        <th>HOUR</th>
                                        <th>ADDRESS</th>
                                        <th>RELOAD</th>
                                    </tr>
                                </thead>
                            </table>
                            <section class="contenido_listas">
                                <table class="table_listas">
                                    <tbody>
                                        <?php echo $html_list; ?>
                                        <!-- <tr>
                                            <td>4th july celebration</td>
                                            <td>2022-01-27</td>
                                            <td>2022-01-27</td>
                                            <td>2022-01-27</td>
                                            <td>HORA</td>
                                            <td><i class="icon-credit-card" style="font-size: 30px; cursor:pointer; color: rgb(0,255,117)"></i></td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </section>
                        </div>
                    </div>
                </section>

            </section>

        </nav>

    </div>

    <script>
        window.onload = function() {
            $('.loader', window.parent.document).fadeOut('fast');
        }
    </script>

</body>



</html>