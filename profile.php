<!DOCTYPE html>

<html>

<head>
    <link href="css/estilos.css" type="text/css" rel="stylesheet">
    <meta charset="utf-8">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <script src="https://kit.fontawesome.com/0bddffe200.js" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>

    <!--Librerias de FullCalendar-->
    <script src="fullcalendar/lib/moment.min.js"></script>
    <link rel="stylesheet" href="fullcalendar/fullcalendar.min.css">
    <script src="fullcalendar/fullcalendar.min.js"></script>

    <!---->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/funciones.js" type="text/jscript"></script>


</head>

<body>

    <?php
    session_start();

    include 'php/conect.php';

    $email = $_SESSION['email'];

    $sql = "SELECT * FROM usuarios WHERE email ='$email'";
    $result = mysqli_query($con, $sql);

    $f_name = "";
    $l_name = "";
    $telefono = "";
    $zip_code = "";
    $county = "";
    $company = "";
    $address1 = "";
    $address2 = "";

    while ($fila  = mysqli_fetch_assoc($result)) {
        $f_name = $fila['f_name'];
        $l_name = $fila['l_name'];
        $telefono = $fila['phone'];
        $zip_code = $fila['zip_code'];
        $county = $fila['city'];
        $company = $fila['company'];
        $address1 = $fila['address'];
        $address2 = $fila['address_2'];
    }

    ?>

    <div class="content home_user">

        <nav class="rigth home_user" style="width: 100%;">

            <section class="programa">
                <br><br>
                <h2>MY PROFILE</h2>
                <br><br>

                <div class="camp_text" style="height: 45vh">
                    <i class="icon-document-editor" onclick="PageChild('edit')" id="edit"></i>
                    <table class="profile">
                        <tr>
                            <td><b>Complete Name</td>
                            <td><b>Telephone</td>
                            <td><b>Zip Code</td>
                            <td><b>County</td>
                        </tr>
                        <tr>
                            <td><?php echo $f_name . " " . $l_name; ?></td>
                            <td><?php echo $telefono; ?></td>
                            <td><?php echo $zip_code; ?></td>
                            <td><?php echo $county; ?></td>
                        </tr>
                        <tr>
                            <td><b>Company</td>
                            <td><b>Address Line 1</td>
                            <td><b>Address Line 2</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><?php echo $company; ?></td>
                            <td><?php echo $address1; ?></td>
                            <td><?php echo $address2; ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><b>Email</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><?php echo $_SESSION['email']; ?></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                    </table>

                </div>
                <br><br><br>
                <table>
                    <tr>
                        <td style="width:300px">
                            <div class="camp_text" style="height: 20vh; width:95%; text-align:center; cursor:pointer" onclick="PageChild('billing')">
                                <br><br>
                                <i class="icon-invoice-1" style="font-size: 45px;"></i>
                                <br><br>
                                <h3>Invoice</h3>
                            </div>
                        </td>
                        <!-- <td style="width:300px">
                            <div class="camp_text" style="height: 20vh; width:95%; text-align:center; cursor:pointer" onclick="PageChild('pay')">
                                <br><br>
                                <i class="icon-credit-card" style="font-size: 45px;"></i>
                                <br><br>
                                <h3>Payment Method</h3>
                            </div>
                        </td> -->
                    </tr>
                </table>


            </section>

        </nav>

    </div>

    <script>
        window.onload = function() {
            $('.loader', window.parent.document).fadeOut('fast');
        }

        function PageChild(page) {
            switch (page) {
                case 'pay':
                    window.open('pay.html', '_self');
                    break;

                case 'billing':
                    window.open('billing.php', '_self');
                    break;

                case 'edit':
                    window.open('edit_profile.php', '_self');
                    break;
            }
        }

        function Next() {
            window.open('create_event.html', '_self');
            $('#drink', window.parent.document).attr('active', 'true');
            $('#home', window.parent.document).attr('active', 'false');
        }

        function ChangeSection(btn) {
            switch (btn) {
                case 'btn_calendar':
                    $('#btn_calendar').attr('state', 'enabled');
                    $('#btn_list').attr('state', '');
                    $('.calendarios').fadeIn(0);
                    $('.listas').fadeOut(0);
                    break;

                case 'btn_list':
                    $('#btn_list').attr('state', 'enabled');
                    $('#btn_calendar').attr('state', '');
                    $('.calendarios').fadeOut(0);
                    $('.listas').fadeIn(0);
                    break;

            }
        }
    </script>

</body>



</html>