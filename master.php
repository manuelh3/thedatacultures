<!DOCTYPE html>

<html lang="us">

<head>
    <link href="css/estilos.css" type="text/css" rel="stylesheet">
    <meta charset="utf-8">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">

    <script src="https://kit.fontawesome.com/0bddffe200.js" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="js/funciones.js" type="text/jscript"></script>


</head>

<body>
    <div class="loader">
        <img src="img/loader.gif">
    </div>

    <?php
    session_start();

    ?>

    <div class="sombra">
        <div class="alert">
            <nav>
                <i class="icon-alert"></i><br><br><br>
                <h2 id="msm_alert">Please must accept the policies</h2><br><br><br>
                <input type="button" id="btn_alert" onclick="CloseAlert()" class="btn" value="OK">
            </nav>
        </div>

        <div class="confirm">
            <nav>
                <i class="icon-alert"></i><br><br><br>
                <h2 id="msm_alert">Are you sure you want to archive this event?</h2><br><br><br>
                <center>
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 50%;"><input type="button" id="btn_alert" onclick="Confirm('false')" class="btn" value="CANCEL"></td>
                            <td style="width: 50%;"><input type="button" id="btn_alert" onclick="Confirm('true')" class="btn" value="TO ACCEPT"></td>
                        </tr>
                    </table>
                </center>
            </nav>
        </div>
        <div class="pay">
            <i class="icon-close" onclick="ClosePayment()" style="font-size: 30px; cursor:pointer;float:right; padding:20px"></i>
            <iframe src="stripe/checkout.html">

            </iframe>
        </div>
    </div>
    <script>
        function ClosePayment() {
            $('.sombra').fadeOut('fast');
            $('.pay').fadeOut('fast');
        }
    </script>

    </div>
    <div id="menu_user">
        <section>
            <ul>
                <!-- <li onclick="Page('billing')">Billing</li> -->
                <li onclick="Page('trash')">Archive Events</li>
                <li onclick="Page('pay')">Payment Method</li>
                <li>
                    <hr>
                </li>
                <li onclick="Page('configure')">Configure</li>
            </ul>
        </section>
    </div>

    <div id="content">
        <nav class="left master">
            <div id="desplegar"><i class="icon-next"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i></div>
            <img src="img/logo_1.png" class="logo_master">
            <br><br><br><br>
            <div class="item" active="true" onclick="Page(this.id)" id="home"><i class="icon-home"></i></div>
            <div class="item" active="false" onclick="Page(this.id)" id="drink"><i class="icon-drink"></i></div>
            <div class="item" active="false" onclick="Page(this.id)" id="billing"><i class="icon-invoice-1"></i></div>
            <div class="item" active="false" onclick="Page(this.id)" id="trash"><i class="icon-file"></i></div>
            <div class="item" active="false" onclick="Page(this.id)" id="configure"><i class="icon-settings"></i></div>

            <br><br><br><br><br><br><br><br>
            <div class="item" active="false" onclick="Salir()"><i class="icon-salida"></i></div>
        </nav>
        <nav class="rigth master">
            <div id="total_info">
                <div id="head_master">
                    <table>
                        <tr>
                            <td><img src="img/img_user.png"></td>
                            <td>
                                <h5><?php echo $_SESSION['name_user']; ?></h5>
                            </td>
                            <td onclick="MenuUser()"><img class="down_arrow" src="img/down_arrow.png"> </td>
                        </tr>
                    </table>
                </div>
                <iframe id="data" src="home_user.php">

                </iframe>

            </div>
        </nav>
    </div>

    <script>
        window.onload = function() {
            $(".loader").fadeOut('fast');
        }

        function MenuUser() {
            $('#menu_user').toggle('fast');
        }

        function Salir() {
            $.ajax({
                cache: false,
                method: 'post',
                url: 'php/close.php',
                success: function(res) {
                    window.open('index.php', '_self');
                }
            });
        }

        //window.history.pushState({},'','/test_rbar/');
    </script>

</body>



</html>