<!DOCTYPE html>

<html>

<head>
    <link href="css/estilos.css" type="text/css" rel="stylesheet">
    <meta charset="utf-8">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <script src="https://kit.fontawesome.com/0bddffe200.js" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>

    <!--Librerias de FullCalendar-->
    <script src="fullcalendar/lib/moment.min.js"></script>
    <link rel="stylesheet" href="fullcalendar/fullcalendar.min.css">
    <script src="fullcalendar/fullcalendar.min.js"></script>

    <!---->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/funciones.js" type="text/jscript"></script>


</head>

<body>

    <?php
    session_start();

    include 'php/conect.php';

    $email = $_SESSION['email'];

    $sql = "SELECT * FROM eventos ORDER BY date_event DESC";
    $result = mysqli_query($con, $sql);

    $name = "";
    $date = "";
    $hour = "";
    $address = "";
    $estado = "";
    $id_event = "";

    $color = "";

    $html = "";
    $html_list = "";
    $html_past = "";

    $date_actual = strtotime(date("Y-m-d", time()));

    while ($fila  = mysqli_fetch_assoc($result)) {
        $name = $fila['name'];
        $date = $fila['date_event'];
        $date_ini = date_create($date);
        $date_set = date_format($date_ini,"m/d/Y");
        $hour = $fila['hour_event'];
        $address = $fila['address1'];
        $estado = $fila['estado'];
        $id_event = $fila['id_evento'];

        switch($estado){

            case 'activo':
                $estado = "ACTIVE";
                $color = "green";

                $html .= '<tr>
                    <td>' . $name . '</td>
                    <td>' . $date_set . '</td>
                    <td style="width: 20%;">' . $hour . '</td>
                    <td>' . $address . '</td>
                    <td style="color:' . $color . '">' . $estado . '</td>
                    </tr>';
            break;

            case 'pasado':
                $estado = "LAST";
                $color = "gray";

                $html_past .= '<tr>
                    <td>' . $name . '</td>
                    <td>' . $date_set . '</td>
                    <td style="width: 20%;">' . $hour . '</td>
                    <td>' . $address . '</td>
                    <td style="color:' . $color . '">' . $estado . '</td>
                    </tr>';
            break;

        }

        if (strtotime($date) > $date_actual) {
            $html_list .= '<table>
        <tr>
            <td>
                <div class="point ' . $color . '"></div>
            </td>
            <td>
                <h5 style="margin-left: 30px">' . $name . '</h5>
            </td>
        </tr>
    </table>';
        }

    }

    ?>

    <script>
        var allDate = [];
        var allName = [];

        var color = "";

        /*rgb(0,255,117) verde*/
        /*rgb(132,133,132) gris*/
        /*rgb(235,83,84) rojo*/

        //var prueba = '[{"start":"2022-01-09","color":"rgb(132, 133, 132)"},{"start":"2022-01-12","color":"rgb(132, 133, 132)"},{"start":"2022-01-20","color":"rgb(0,255,117)"}]';
        //var fecha =  JSON.parse(prueba);

        var bandera = '<i class="icon-Bandera"></i>';
        var textoCont = '<h1 class="textEvents">Events <span id="nEvents">1</span></h1>'

        $(document).ready(function() {

            $.ajax({
                cache: false,
                method: 'post',
                url: 'php/get_event_admin.php',
                success: function(res) {

                    var fechas = JSON.parse(res);

                    $("#CalendarioWeb").fullCalendar({
                        dayRender: function(date, cell) {
                            var theDate = $(cell).data('date');
                            var textDate = theDate.toString();
                            var onlyDate = textDate.substring(8);
                            allName.push(textDate);
                            allDate.push(onlyDate);
                            if (allDate.length >= 42) {
                                CargarDate(allDate, allName, fechas);
                            }
                        },
                        events: fechas
                    });
                }
            });

        });

        var flag = false;
        var flag2 = false;
        var cont_day = 0;

        function CargarDate(date, dateName, calendar) {
            for (i = 0; i < 42; i++) {
                document.getElementById(i).innerHTML = date[i];
                $('#' + i).attr('name', dateName[i]);

                if (date[i] > 24 && flag == false) {
                    $('#' + i).attr('estado', 'out');
                } else {
                    $('#' + i).attr('estado', '');
                    flag = true;

                    if (date[i] > 28) {
                        flag2 = true;
                    }

                }

                if (date[i] < 27 && flag2 == true) {
                    $('#' + i).attr('estado', 'out');
                }
            }

            allDate = [];
            allName = [];
            flag = false;
            flag2 = false;
            cont_day = 0;

            for (i = 1; i < calendar.length; i++) {

                switch (calendar[i].color) {
                    case "rgb(0,255,117)":
                        color = "green";
                        break;

                    case "rgb(132,133,132)":
                        color = "past";
                        break;

                    case "rgb(235,83,84)":
                        color = "red";
                        break;
                }

                $('td[name="' + calendar[i].start + '"]').attr("estado", color);
                $('td[name="' + calendar[i].start + '"]').append(bandera);
                $('td[name="' + calendar[i].start + '"]').append(textoCont);
            }



        }
    </script>

    <div class="content home_user">

        <nav class="left home_user">
            <div class="calendario_1">
                <div id="CalendarioWeb">

                </div>
            </div>
            <br><br><br><br>

        </nav>

        <nav class="rigth home_user">

            <section class="programa">

                <table>

                    <tr>
                        <td>
                            <div class="btn_2" id="btn_calendar" state="enabled" onclick="ChangeSection(this.id)">
                                <table>
                                    <tr>
                                        <td><i class="icon-Calendar"></i></td>
                                        <td>
                                            <h5>Calendar</h5>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td>
                            <div class="btn_2" id="btn_list" onclick="ChangeSection(this.id)">
                                <table>
                                    <tr>
                                        <td><i class="icon-planning"></i></td>
                                        <td>
                                            <h5>Events List</h5>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td>
                            <div class="btn_2" id="btn_list_past" onclick="ChangeSection(this.id)">
                                <table>
                                    <tr>
                                        <td><i class="icon-planning"></i></td>
                                        <td>
                                            <h5>Events Past</h5>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>

                <br>
                <section class="calendarios">
                    <div class="calendario_2">
                        <div id="inside_calendario_2">
                            <table class="table1">
                                <thead>
                                    <tr>
                                        <th>Sunday</th>
                                        <th>Monday</th>
                                        <th>Tuesday</th>
                                        <th>Wednesday</th>
                                        <th>Thursday</th>
                                        <th>Friday</th>
                                        <th>Saturday</th>
                                    </tr>
                                </thead>
                            </table>
                            <br><br>
                            <table class="table2" style="overflow-y: scroll;">
                                <tbody>
                                    <tr>
                                        <td id="0">1</td>
                                        <td id="1">2</td>
                                        <td id="2">3</td>
                                        <td id="3">4</td>
                                        <td id="4">5</td>
                                        <td id="5">6</td>
                                        <td id="6">7</td>
                                    </tr>

                                    <tr>
                                        <td id="7">8</td>
                                        <td id="8">9</td>
                                        <td id="9">10</td>
                                        <td id="10">11</td>
                                        <td id="11">12</td>
                                        <td id="12">13</td>
                                        <td id="13">14</td>
                                    </tr>

                                    <tr>
                                        <td id="14">15</td>
                                        <td id="15">16</td>
                                        <td id="16">17</td>
                                        <td id="17">18</td>
                                        <td id="18">19</td>
                                        <td id="19">20</td>
                                        <td id="20">21</td>
                                    </tr>

                                    <tr>
                                        <td id="21">22</td>
                                        <td id="22">23</td>
                                        <td id="23">24</td>
                                        <td id="24">25</td>
                                        <td id="25">26</td>
                                        <td id="26">27</td>
                                        <td id="27">28</td>
                                    </tr>

                                    <tr>
                                        <td id="28">29</td>
                                        <td id="29">30</td>
                                        <td id="30">31</td>
                                        <td id="31">1</td>
                                        <td id="32">2</td>
                                        <td id="33">3</td>
                                        <td id="34">4</td>
                                    </tr>

                                    <tr>
                                        <td id="35">29</td>
                                        <td id="36">30</td>
                                        <td id="37">31</td>
                                        <td id="38">1</td>
                                        <td id="39">2</td>
                                        <td id="40">3</td>
                                        <td id="41">4</td>
                                    </tr>

                                </tbody>

                            </table>
                        </div>
                    </div>
                </section>
                <section class="listas" style="display: none;">
                    <div class="calendario_2">
                        <div id="inside_calendario_2">
                            <table class="table1 title">
                                <thead>
                                    <tr>
                                        <th>EVENT NAME</th>
                                        <th>DATE</th>
                                        <th style="width: 20%;">HOUR</th>
                                        <th>ADDRESS</th>
                                        <th>STATE</th>
                                        <!-- <th>DELETE</th> -->
                                    </tr>
                                </thead>
                            </table>
                            <section class="contenido_listas">
                                <table class="table_listas">
                                    <tbody>
                                        <?php echo $html; ?>
                                        <!-- <tr>
                                    <td>4th july celebration</td>
                                    <td>2022-01-27</td>
                                    <td style="width: 20%;">Hour: Start 5:00 pm End 7:00 pm</td>
                                    <td>Textas</td>
                                    <td>ACTIVE</td>
                                    <td><i class="icon-Eliminar" style="font-size: 30px; cursor:pointer"></i></td>
                                </tr> -->
                                    </tbody>
                                </table>
                            </section>
                        </div>
                    </div>
                </section>

                <section class="listas_past" style="display: none;">
                    <div class="calendario_2">
                        <div id="inside_calendario_2">
                            <table class="table1 title">
                                <thead>
                                    <tr>
                                        <th>EVENT NAME</th>
                                        <th>DATE</th>
                                        <th style="width: 20%;">HOUR</th>
                                        <th>ADDRESS</th>
                                        <th>STATE</th>
                                    </tr>
                                </thead>
                            </table>
                            <section class="contenido_listas">
                                <table class="table_listas">
                                    <tbody>
                                    <?php echo $html_past; ?>
                                        <!-- <tr>
                                            <td>4th july celebration</td>
                                            <td>2022-01-27</td>
                                            <td style="width: 20%;">Hour: Start 5:00 pm End 7:00 pm</td>
                                            <td>Textas</td>
                                            <td>PAST</td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </section>
                        </div>
                    </div>
                </section>

            </section>

        </nav>

    </div>

    <script>
        window.onload = function() {
            $('.loader', window.parent.document).fadeOut('fast');
        }

        function Next() {
            window.open('create_event.php', '_self');
            $('#drink', window.parent.document).attr('active', 'true');
            $('#home', window.parent.document).attr('active', 'false');
        }

        function ChangeSection(btn) {
            switch (btn) {
                case 'btn_calendar':
                    $('#btn_calendar').attr('state', 'enabled');
                    $('#btn_list').attr('state', '');
                    $('#btn_list_past').attr('state', '');
                    $('.calendarios').fadeIn(0);
                    $('.listas').fadeOut(0);
                    $('.listas_past').fadeOut(0);
                    break;

                case 'btn_list':
                    $('#btn_list').attr('state', 'enabled');
                    $('#btn_calendar').attr('state', '');
                    $('#btn_list_past').attr('state', '');
                    $('.calendarios').fadeOut(0);
                    $('.listas').fadeIn(0);
                    $('.listas_past').fadeOut(0);
                    break;

                case 'btn_list_past':
                    $('#btn_list_past').attr('state', 'enabled');
                    $('#btn_list').attr('state', '');
                    $('#btn_calendar').attr('state', '');
                    $('.calendarios').fadeOut(0);
                    $('.listas').fadeOut(0);
                    $('.listas_past').fadeIn(0);
                    break;

            }
        }
    </script>

</body>



</html>