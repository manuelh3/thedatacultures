<!DOCTYPE html>

<html>

<head>
    <link href="css/estilos.css" type="text/css" rel="stylesheet">
    <meta charset="utf-8">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <script src="https://kit.fontawesome.com/0bddffe200.js" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>

    <!--Librerias de FullCalendar-->
    <script src="fullcalendar/lib/moment.min.js"></script>
    <link rel="stylesheet" href="fullcalendar/fullcalendar.min.css">
    <script src="fullcalendar/fullcalendar.min.js"></script>

    <!---->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/funciones.js" type="text/jscript"></script>


</head>

<body>

    <?php

    include 'php/conect.php';

    $sql = "SELECT * FROM wine";
    $result = mysqli_query($con, $sql);

    $sql2 = "SELECT * FROM guest";
    $result2 = mysqli_query($con, $sql2);

    $sql3 = "SELECT * FROM wines";
    $result3 = mysqli_query($con, $sql3);

    $id = "";
    $name = "";
    $purple = "";

    $id_guest = "";
    $min = "";
    $max = "";
    $guest = "";

    $id_wine = "";
    $name_wine = "";

    $html_spirits = "";
    $html_wines = "";
    $html_guest = "";


    while ($fila  = mysqli_fetch_assoc($result)) {
        $id = $fila['id_wine'];
        $name = $fila['name'];
        $purple = $fila['purple'];

        $html_spirits .= '<nav class="box-tag" purple="' . $purple . '" style="margin-left:10px">
        <label>' . $name . '</label>
        <i class="icon-close admin" onclick="DeleteWines('.$id.')"></i>
    </nav>';
    }

    while ($fila2  = mysqli_fetch_assoc($result2)) {
        $id_guest = $fila2['id_guest'];
        $min = $fila2['min'];
        $max = $fila2['max'];
        $guest = $fila2['guest_s'];

        $html_guest .= '<nav class="box-tag" purple="false" style="margin-left:10px">
        <label>' . $min . '-' . $max . ' ($' . $guest . ' Minimun)</label>
        <i class="icon-close admin" onclick="DeleteGuest('.$id_guest.')"></i>
    </nav>';
    }

    while ($fila3  = mysqli_fetch_assoc($result3)) {
        $id_wine = $fila3['id_wines'];
        $name_wine = $fila3['name'];

        $html_wines .= '<nav class="box-tag" style="margin-left:10px">
        <label>' . $name_wine . '</label>
        <i class="icon-close admin" onclick="DeleteWines2('.$id_wine.')"></i>
    </nav>';
    }

    ?>

    <div class="content home_user">

        <nav class="rigth home_user" style="width: 100%;">

            <section class="programa">
                <br>
                <section class="listas">
                    <form action="javascript:ConfigureWines('add',0)">
                        <table>
                            <tr>
                                <td> <br>
                                    <h2>Spirits</h2>
                                </td>
                                <td>
                                    <div class="input_text2" style="margin-left: 20px;">
                                        <br>
                                        <input type="text" placeholder="Name Spirits..." id="name_wine" required>
                                    </div>
                                </td>
                                <td>
                                    <br>
                                    <input type="submit" class="btn" value="Add" style="margin-left: 18px; width:200px ; text-align: center; height:42px; margin-top:5px" readonly>
                                </td>
                                <td>
                                    <br>
                                    <h2 style="margin-left: 70px">Premium</h2>
                                    <label class="checkbox" style="margin-left: 180px; margin-top:-25px">
                                        <input type="checkbox" id="check_purple">
                                        <i class="fas fa-check-square"></i>
                                    </label>
                                </td>
                            </tr>
                            <tr>

                            </tr>
                        </table>
                    </form>
                    <br>
                    <div class="user_more_info" style="background-color: white; border-top:2px solid rgba(0,0,0,0.2);border-right:2px solid rgba(0,0,0,0.2);border-left:2px solid rgba(0,0,0,0.2);height:30vh; overflow-y:scroll">
                        <section class="items_wine" style="padding: 20px;">
                            <?php echo $html_spirits; ?>
                        </section>
                    </div>
                    <!-- Sección para vinos -->
                    <form action="javascript:ConfigureWines2('add',0)">
                        <table>
                            <tr>
                                <td> <br>
                                    <h2>Wines</h2>
                                </td>
                                <td>
                                    <div class="input_text2" style="margin-left: 20px;">
                                        <br>
                                        <input type="text" placeholder="Name Wine..." id="name_wine2" required>
                                    </div>
                                </td>
                                <td>
                                    <br>
                                    <input type="submit" class="btn" value="Add" style="margin-left: 18px; width:200px ; text-align: center; height:42px; margin-top:5px" readonly>
                                </td>
                            </tr>
                            <tr>

                            </tr>
                        </table>
                    </form>
                    <br>
                    <div class="user_more_info" style="background-color: white; border-top:2px solid rgba(0,0,0,0.2);border-right:2px solid rgba(0,0,0,0.2);border-left:2px solid rgba(0,0,0,0.2);height:30vh; overflow-y:scroll">
                        <section class="items_wine" style="padding: 20px;">
                            <?php echo $html_wines; ?>
                        </section>
                    </div>

                    
                    <!-- Sección para precios por cantidad de clientes -->
                    <form action="javascript:AddGuest()">
                    <table>
                        <tr>
                            <td> <br>
                                <h2>Guest</h2>
                            </td>
                            <td>
                                <div class="input_text2" style="margin-left: 20px;">
                                    <br>
                                    <input type="text" placeholder="Min..." id="min_guest" required>
                                </div>
                            </td>
                            <td>
                                <div class="input_text2" style="margin-left: 20px;">
                                    <br>
                                    <input type="text" placeholder="Max..." id="max_guest" required>
                                </div>
                            </td>
                            <td>
                                <div class="input_text2" style="margin-left: 20px;">
                                    <br>
                                    <input type="text" placeholder="Guest Standard" id="standard_guest" required>
                                </div>
                            </td>
                            <td>
                                <div class="input_text2" style="margin-left: 20px;">
                                    <br>
                                    <input type="text" placeholder="Guest Ultimate" id="ultimate_guest" required>
                                </div>
                            </td>
                            <td>
                                <br>
                                <input type="submit" class="btn" value="Add" style="margin-left: 18px;width:200px ; text-align: center; height:42px; margin-top:5px" readonly>
                            </td>
                        </tr>
                        <tr>

                        </tr>
                    </table>
                    </form>
                    <br>
                    <div class="user_more_info" style="background-color: white; border-top:2px solid rgba(0,0,0,0.2);border-right:2px solid rgba(0,0,0,0.2);border-left:2px solid rgba(0,0,0,0.2);height:30vh; overflow-y:scroll">
                        <section class="items_wine" style="padding: 20px;">
                            <?php echo $html_guest; ?>
                            <!-- <nav class="box-tag" purple="false">
                                <label>1-50 ($3300 Minimun)</label>
                                <i class="icon-close admin"></i>
                            </nav> -->

                        </section>
                    </div>
                </section>

            </section>

        </nav>

    </div>

    <script>
        window.onload = function() {
            $('.loader', window.parent.document).fadeOut('fast');
        }

        function ConfigureWines(type, id_delete) {
            var check = document.getElementById('check_purple').checked;
            var obj_function = JSON.stringify(type);
            var obj_name = JSON.stringify(document.getElementById('name_wine').value);
            var obj_purple = JSON.stringify(check.toString());

            $.ajax({
                cache: false,
                type: 'POST',
                url: 'php/configure_wines.php',
                data: {
                    obj_function: obj_function,
                    obj_name: obj_name,
                    obj_purple:obj_purple
                },
                success: function() {
                    location.reload();
                }
            });
        }

        function DeleteWines(id_delete) {
           
            var obj_function = JSON.stringify('delete');
            var obj_id = JSON.stringify(id_delete);

            $.ajax({
                cache: false,
                type: 'POST',
                url: 'php/configure_wines.php',
                data: {
                    obj_function: obj_function,
                    obj_id:obj_id
                },
                success: function() {
                    location.reload();
                }
            });
        }

        function AddGuest(){
            var obj_function = JSON.stringify('add');
            var obj_min = JSON.stringify(document.getElementById('min_guest').value);
            var obj_max = JSON.stringify(document.getElementById('max_guest').value);
            var obj_standard = JSON.stringify(document.getElementById('standard_guest').value);
            var obj_ultimate = JSON.stringify(document.getElementById('ultimate_guest').value);

            $.ajax({
                cache: false,
                type: 'POST',
                url: 'php/configure_guest.php',
                data: {
                    obj_function: obj_function,
                    obj_min: obj_min,
                    obj_max:obj_max,
                    obj_standard:obj_standard,
                    obj_ultimate:obj_ultimate
                },
                success: function() {
                    location.reload();
                }
            });
        }

        function DeleteGuest(id_delete){

            $('.sombra', window.parent.document).fadeIn('fast');
            $('.confirm', window.parent.document).fadeIn('fast');
            localStorage.setItem('id_guest_delete',id_delete);
            
        //    var obj_function = JSON.stringify('delete');
        //    var obj_id = JSON.stringify(id_delete);

        //    $.ajax({
        //        cache: false,
        //        type: 'POST',
        //        url: 'php/configure_guest.php',
        //        data: {
        //            obj_function: obj_function,
        //            obj_id:obj_id
        //        },
        //        success: function() {
        //            location.reload();
        //        }
        //    });
       }


       function ConfigureWines2(type, id_delete) {

            var obj_function = JSON.stringify(type);
            var obj_name = JSON.stringify(document.getElementById('name_wine2').value);

            $.ajax({
                cache: false,
                type: 'POST',
                url: 'php/configure_wines2.php',
                data: {
                    obj_function: obj_function,
                    obj_name: obj_name
                },
                success: function() {
                    location.reload();
                }
            });
        }

        function DeleteWines2(id_delete) {
           
            var obj_function = JSON.stringify('delete');
            var obj_id = JSON.stringify(id_delete);

            $.ajax({
                cache: false,
                type: 'POST',
                url: 'php/configure_wines2.php',
                data: {
                    obj_function: obj_function,
                    obj_id:obj_id
                },
                success: function() {
                    location.reload();
                }
            });
        }
        
    </script>

</body>



</html>