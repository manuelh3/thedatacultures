<?php

require 'vendor/autoload.php';

session_start();

$guest = $_SESSION['guest'];
$amount = intval($guest);
$name = $_SESSION['name_event'];

// This is your test secret API key.
\Stripe\Stripe::setApiKey('sk_test_51KVpneBOTmiEKrjh8VbqwwhQpNKYAqdSmEIbeIt3LkPI8l5VFCouoPoso4Na5MSxCdmSZCr3P3dSETH5Phb9yuR000RNOMOrh0');

header('Content-Type: application/json');

try {
    // retrieve JSON from POST body
    $jsonStr = file_get_contents('php://input');
    $jsonObj = json_decode($jsonStr);

    // Create a PaymentIntent with amount and currency
    $paymentIntent = \Stripe\PaymentIntent::create([
        'amount' => $amount,
        'currency' => 'usd',
        'description' => "$name",
        'automatic_payment_methods' => [
            'enabled' => true,
        ],
    ]);

    $output = [
        'clientSecret' => $paymentIntent->client_secret,
    ];

    echo json_encode($output);
} catch (Error $e) {
    http_response_code(500);
    echo json_encode(['error' => $e->getMessage()]);
}