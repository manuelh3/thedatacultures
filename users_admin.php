<!DOCTYPE html>

<html>

<head>
    <link href="css/estilos.css" type="text/css" rel="stylesheet">
    <meta charset="utf-8">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <script src="https://kit.fontawesome.com/0bddffe200.js" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>

    <!--Librerias de FullCalendar-->
    <script src="fullcalendar/lib/moment.min.js"></script>
    <link rel="stylesheet" href="fullcalendar/fullcalendar.min.css">
    <script src="fullcalendar/fullcalendar.min.js"></script>

    <!---->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/funciones.js" type="text/jscript"></script>


</head>

<body>

    <?php

    include 'php/conect.php';

    $sql = "SELECT * FROM usuarios";
    $result = mysqli_query($con, $sql);

    $id_user = "";
    $user_f_name = "";
    $user_l_name = "";
    $company = "";
    $email = "";
    $events = "";
    $html_events = "";
    $phone = "";

    $address = "";
    $state = "";
    $city ="";
    $postal = "";

    $estado_user = "";

    $html_list = "";
    $cont = 0;


    while ($fila  = mysqli_fetch_assoc($result)) {
        $id_user = $fila['id_usuario'];
        $user_f_name = $fila['f_name'];
        $user_l_name = $fila['l_name'];
        $company = $fila['company'];
        $email = $fila['email'];
        $phone = $fila['phone'];
        $estado_user = $fila['state'];

        $address = $fila['address'];
        $state = $fila['address_2'];
        $city =$fila['city'];
        $postal = $fila['zip_code'];

        $events = NumberEvents($email);
        $html_events = HtmlEvents($email);
        $cont++;

        $html_list .= '<tr>
                        <td>' . $user_f_name . ' ' . $user_l_name . '</td>
                        <td>' . $company . '</td>
                        <td>' . $email . '</td>
                        <td>' . $events . '</td>
                        <td>' . $phone . '</td>
                        <td><img class="down_arrow" src="img/down_arrow.png" onclick="Desplegar('.$cont.')"></td>
                        <td>
                        
                        <p style="colorrgb(139,197,129); text-align:center;cursor:pointer" id="state_'.$id_user.'" onclick="ChangeStateUser('.$id_user.')">'.$estado_user.'</p>
                        
                    <nav class="menu_balance" >
                        <br>
                        <table>
                            <tr>
                                <td><b>User Admin</b></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="paid">Active</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="unpaid">Disabled</div>
                                </td>
                            </tr>
                        </table>
                    </nav>
                        </td>
                    </tr>
                    <tr  class="info_more" id="info_'.$cont.'">
                        <td colspan="7">
                            <div class="user_more_info">
                                <br><br>
                                <table class="table1 subtable">
                                    <tr>
                                        <td>ADDRESS</td>
                                        <td>COUNTY</td>
                                        <td>CITY</td>
                                        <td>POSTAL CODE</td>
                                        <td></td>
                                        <td><img class="down_arrow up" onclick="Retraer('.$cont.')" src="img/down_arrow.png"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>' . $address . '</td>
                                        <td>' . $state . '</td>
                                        <td>' . $city . '</td>
                                        <td>' . $postal . '</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                                <section class="events_user">
                                    <nav>
                                        <h4>EVENTS</h4>
                                        <table class="table1 subtable event">
                       
                                            <tr>
                                                <td></td>
                                                <td>NAME EVENT</td>
                                                <td>DATE</td>
                                                <td>CITY</td>
                                                <td>EXPERIECE</td>
                                                <td>STATE</td>
                                            </tr>
                                            '.$html_events.'
                                        </table>
                                    </nav>
                                </section>
                            </div>
                        </td>
                    </tr>';
    }

    function NumberEvents($correo)
    {
        include 'php/conect.php';

        $number_events = 0;

        $sql = "SELECT * FROM eventos WHERE email_user='$correo' AND estado='activo'";
        $result = mysqli_query($con, $sql);

        while ($fila  = mysqli_fetch_assoc($result)) {
            $number_events++;
        }

        return $number_events;
    }

    function HtmlEvents($correo)
    {
        include 'php/conect.php';

        $html_e = '';
        $cont = 0;

        $name ="";
        $date ="";
        $city = "";
        $experience ="";
        $state = "";

        $sql = "SELECT * FROM eventos WHERE email_user='$correo' AND estado='activo'";
        $result = mysqli_query($con, $sql);

        while ($fila  = mysqli_fetch_assoc($result)) {

            $cont++;
            $name = $fila['name'];
            $date =$fila['date_event'];
            $date_ini = date_create($date);
            $date_set = date_format($date_ini,"m/d/Y");
            $city = $fila['city'];
            $experience =$fila['experience'];
            $state = $fila['estado'];

            $html_e .= '<tr>
            <td>'.$cont.'</td>
            <td>'.$name.'</td>
            <td>'.$date_set.'</td>
            <td>'.$city.'</td>
            <td>'.$experience.'</td>
            <td>'.$state.'</td>
        </tr>';
        }

        return $html_e;
    }

    ?>

    <div class="content home_user">

        <nav class="rigth home_user" style="width: 100%;">

            <section class="programa">
                <br><br><br>
                <section class="listas">
                    <div class="calendario_2">
                        <div id="inside_calendario_2">
                            <table class="table1 title">
                                <thead>
                                    <tr>
                                        <th>NAME</th>
                                        <th>COMPANY</th>
                                        <th>EMAIL</th>
                                        <th>N° EVENTS</th>
                                        <th>PHONE</th>
                                        <th>MORE</th>
                                        <th>STATE</th>
                                    </tr>
                                </thead>
                            </table>
                            <section class="contenido_listas">
                                <table class="table_listas">
                                    <tbody>
                                        <?php echo $html_list; ?>
                                    </tbody>
                                </table>
                            </section>
                        </div>
                    </div>
                </section>

            </section>

        </nav>

    </div>

    <script>
        window.onload = function() {
            $('.loader', window.parent.document).fadeOut('fast');
        }

        function Next() {
            window.open('create_event.html', '_self');
            $('#drink', window.parent.document).attr('active', 'true');
            $('#home', window.parent.document).attr('active', 'false');
        }

        function ChangeSection(btn) {
            switch (btn) {
                case 'btn_calendar':
                    $('#btn_calendar').attr('state', 'enabled');
                    $('#btn_list').attr('state', '');
                    $('.calendarios').fadeIn(0);
                    $('.listas').fadeOut(0);
                    break;

                case 'btn_list':
                    $('#btn_list').attr('state', 'enabled');
                    $('#btn_calendar').attr('state', '');
                    $('.calendarios').fadeOut(0);
                    $('.listas').fadeIn(0);
                    break;

            }
        }

        function Desplegar(id_info){
            var idFull = "info_"+ id_info;
            $('#'+ idFull).slideDown('slow');
        }

        function Retraer(id_info){
            var idFull = "info_"+ id_info;
            $('#'+ idFull).fadeOut(0);
        }

        function ChangeStateUser(idUser){
           var obj_id = JSON.stringify(idUser);
           var obj_state = JSON.stringify(document.getElementById('state_'+ idUser).innerHTML);

           $.ajax({
               cache: false,
               type: 'POST',
               url: 'php/change_user.php',
               data: {
                   obj_id:obj_id,
                   obj_state:obj_state
               },
               success: function() {
                   location.reload();
               }
           });
        }
    </script>

</body>



</html>