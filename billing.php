<!DOCTYPE html>

<html>

<head>
    <link href="css/estilos.css" type="text/css" rel="stylesheet">
    <meta charset="utf-8">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <link rel="stylesheet" href="css/style2.css">
    <script src="js/jquery.min.js"></script>

    <!--Librerias de FullCalendar-->
    <script src="fullcalendar/lib/moment.min.js"></script>
    <link rel="stylesheet" href="fullcalendar/fullcalendar.min.css">
    <script src="fullcalendar/fullcalendar.min.js"></script>

    <!---->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="stylesheet" href="css/style.css"> -->
    <script src="js/funciones.js" type="text/jscript"></script>


</head>

<body>

<?php

    session_start();

    $email = $_SESSION['email'];

    include 'php/conect.php';

    $sql = "SELECT * FROM eventos WHERE email_user = '$email' AND billing_up = 'true'";
    $result = mysqli_query($con, $sql);

    $name = "";
    $date = "";
    $file = "";
    $file1 = "";
    $file2 = "";
    $url = "";
    $url2 = "";
    $url3 = "";

    $download1 = "";
    $download2 = "";
    $download3 = "";

    $html_list = "";


    while ($fila  = mysqli_fetch_assoc($result)) {
        $name = $fila['name'];
        $date = $fila['date_event'];
        $url = $fila['billing_url'];
        $url2 = $fila['billing_urla'];
        $url3 = $fila['billing_urlb'];

        if($url != "") $download1 = 'download';
        else $download1 = "disabled";

        if($url2 != "") $download2 = 'download';
        else $download2 = "disabled";

        if($url3 != "") $download3 = 'download';
        else $download3 = "disabled";

        $file = $fila['billing_up'];
        $file2 = $fila['billing_upa'];
        $file3 = $fila['billing_upb'];

        $date_ini = date_create($date);
        $date_set = date_format($date_ini,"m/d/Y");

        $html_list .= '<tr>
        <td>'.$name.'</td>
        <td>'.$date_set.'</td>
        <td>'.$date_set.'</td>
        <td><a href="'.$url.'" '.$download1.'><i class="icon-Descargar" style="font-size: 30px" file="'.$file.'"></i></a></td>
        <td><a href="'.$url2.'" '.$download2.'><i class="icon-Descargar" style="font-size: 30px" file="'.$file2.'"></i></a></td>
        <td><a href="'.$url3.'" '.$download3.'><i class="icon-Descargar" style="font-size: 30px" file="'.$file3.'"></i></a></td>';
    }

    ?>

    <div class="content home_user">

        <nav class="rigth home_user" style="width: 100%;">

            <section class="programa">
                <br><br>
                <h2>INVOICE</h2>
                <br><br>

                <section class="listas">
                    <div class="calendario_2">
                        <div id="inside_calendario_2">
                            <table class="table1 title">
                                <thead>
                                    <tr>
                                        <th>EVENT NAME</th>
                                        <th>EVENT DATE</th>
                                        <th>INVOICE DATE</th>
                                        <th>INVOICE 1</th>
                                        <th>INVOICE 2</th>
                                        <th>INVOICE 3</th>
                                    </tr>
                                </thead>
                            </table>
                            <section class="contenido_listas">
                                <table class="table_listas">
                                    <tbody>
                                        <?php echo $html_list; ?>
                                        <!-- <tr>
                                            <td>4th july celebration</td>
                                            <td>2022-01-27</td>
                                            <td>2022-01-27</td>
                                            <td>FP000586</td>
                                            <td><i class="icon-Guardar" style="font-size: 30px; cursor:pointer"></i></td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </section>
                        </div>
                    </div>
                </section>

            </section>

        </nav>

    </div>

    <script>
        window.onload = function() {
            $('.loader', window.parent.document).fadeOut('fast');
        }
    </script>

</body>



</html>