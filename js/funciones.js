
function tConvert(time) {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
        time = time.slice(1);  // Remove full string match value
        time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); // return adjusted time or original string
}

function NextPage(page) {

    switch (page) {

        case 1:
            window.open('create_event.php', '_self');
            break;

        case 2:
            localStorage.setItem("nameEvent", document.getElementById('nameEvent').value);
            localStorage.setItem("address1", document.getElementById('address1').value);
            localStorage.setItem("address2", document.getElementById('address2').value);
            localStorage.setItem("county", document.getElementById('county').value);
            localStorage.setItem("city", document.getElementById('city').value);
            localStorage.setItem("zip_code", document.getElementById('zip_code').value);

            var select = document.getElementById('guest');
            var value = select.options[select.selectedIndex].id;
            var value_amount = select.options[select.selectedIndex].value;

            localStorage.setItem('guest', value);
            localStorage.setItem('guest_amount', value_amount);
            localStorage.setItem('dateEvent', document.getElementById('dateInfo').value);
            localStorage.setItem('ini_hour', document.getElementById('time1').value);
            localStorage.setItem('fin_hour', document.getElementById('time2').value);
            localStorage.setItem('hourEvent', document.getElementById('fullTime').value);

            Payment('archivado');

            break;
    }

}

function Reload(idEvent) {
    $('#drink', window.parent.document).attr('active', 'true');
    $('#home', window.parent.document).attr('active', 'false');
    $('#billing', window.parent.document).attr('active', 'false');
    $('#trash', window.parent.document).attr('active', 'false');
    $('#configure', window.parent.document).attr('active', 'false');

    var obj_idEvent = JSON.stringify(idEvent);

    $.ajax({
        cache: false,
        method: 'post',
        url: 'php/reload_event.php',
        data: { obj_idEvent: obj_idEvent },
        success: function (res) {
            var data = JSON.parse(res);

            localStorage.setItem("nameEvent", data.nombre);
            localStorage.setItem("address1", data.address1);
            localStorage.setItem("address2", data.address2);
            localStorage.setItem("county", data.conty);
            localStorage.setItem("city", data.city);
            localStorage.setItem("zip_code", data.zip_code);
            localStorage.setItem('guest_amount', '1 - 50 ($3.300 Minimum)');
            localStorage.setItem('dateEvent', data.date);
            localStorage.setItem('ini_hour', data.time1);
            localStorage.setItem('fin_hour', data.time2);
            localStorage.setItem('hourEvent', data.hour_event);

            $('#data', window.parent.document).attr('src', 'create_event.php');

        }
    });
}


function Page(menu) {

    switch (menu) {
        case 'home':
            $('#data').attr('src', 'home_user.php');
            $('#' + menu).attr('active', 'true');
            $('#drink').attr('active', 'false');
            $('#billing').attr('active', 'false');
            $('#trash').attr('active', 'false');
            $('#configure').attr('active', 'false');
            $('.loader', window.parent.document).fadeIn(0);
            break;

        case 'drink':
            $('#data').attr('src', 'create_event.php');
            $('#' + menu).attr('active', 'true');
            $('#home').attr('active', 'false');
            $('#billing').attr('active', 'false');
            $('#trash').attr('active', 'false');
            $('#configure').attr('active', 'false');
            $('.loader', window.parent.document).fadeIn(0);
            break;

        case 'billing':
            $('#data').attr('src', 'billing.php');
            $('#' + menu).attr('active', 'true');
            $('#home').attr('active', 'false');
            $('#drink').attr('active', 'false');
            $('#trash').attr('active', 'false');
            $('#configure').attr('active', 'false');
            $('.loader', window.parent.document).fadeIn(0);
            break;

        case 'trash':
            $('#data').attr('src', 'trash.php');
            $('#' + menu).attr('active', 'true');
            $('#home').attr('active', 'false');
            $('#drink').attr('active', 'false');
            $('#configure').attr('active', 'false');
            $('#billing').attr('active', 'false');
            $('.loader', window.parent.document).fadeIn(0);
            break;

        case 'configure':
            $('#data').attr('src', 'profile.php');
            $('#' + menu).attr('active', 'true');
            $('#home').attr('active', 'false');
            $('#drink').attr('active', 'false');
            $('#trash').attr('active', 'false');
            $('#billing').attr('active', 'false');
            $('.loader', window.parent.document).fadeIn(0);
            break;

        case 'pay':
            $('#data').attr('src', 'pay.html');
            $('.loader', window.parent.document).fadeIn(0);
            break;
    }

}

function PageAdmin(menu) {

    switch (menu) {
        case 'home':
            $('#data').attr('src', 'home_admin.php');
            $('#' + menu).attr('active', 'true');
            $('#users').attr('active', 'false');
            $('#calendar').attr('active', 'false');
            $('#pay').attr('active', 'false');
            $('#billing').attr('active', 'false');
            $('.loader', window.parent.document).fadeIn(0);
            break;

        case 'users':
            $('#data').attr('src', 'users_admin.php');
            $('#' + menu).attr('active', 'true');
            $('#home').attr('active', 'false');
            $('#calendar').attr('active', 'false');
            $('#pay').attr('active', 'false');
            $('#billing').attr('active', 'false');
            $('.loader', window.parent.document).fadeIn(0);
            break;

        case 'calendar':
            $('#data').attr('src', 'calendar_admin.php');
            $('#' + menu).attr('active', 'true');
            $('#home').attr('active', 'false');
            $('#users').attr('active', 'false');
            $('#pay').attr('active', 'false');
            $('#billing').attr('active', 'false');
            $('.loader', window.parent.document).fadeIn(0);
            break;

        case 'pay':
            $('#data').attr('src', 'pay_admin.php');
            $('#' + menu).attr('active', 'true');
            $('#home').attr('active', 'false');
            $('#users').attr('active', 'false');
            $('#calendar').attr('active', 'false');
            $('#billing').attr('active', 'false');
            $('.loader', window.parent.document).fadeIn(0);
            break;

        case 'billing':
            $('#data').attr('src', 'billing_admin.php');
            $('#' + menu).attr('active', 'true');
            $('#home').attr('active', 'false');
            $('#users').attr('active', 'false');
            $('#callendar').attr('active', 'false');
            $('#pay').attr('active', 'false');
            $('.loader', window.parent.document).fadeIn(0);
            break;
    }

}

function ReturnConfirm(answer) {

    if (answer == 'true') {
        var obj_function = JSON.stringify('delete');
        var obj_id = JSON.stringify(localStorage.getItem('id_guest_delete'));

        $.ajax({
            cache: false,
            type: 'POST',
            url: 'php/configure_guest.php',
            data: {
                obj_function: obj_function,
                obj_id: obj_id
            },
            success: function () {
                localStorage.setItem('id_guest_delete', '');
                $('.sombra', window.parent.document).fadeOut('fast');
                $('.confirm', window.parent.document).fadeOut('fast');
                $('#data', window.parent.document).attr('src', $('#data', window.parent.document).attr('src'));
            }
        });
    }
    else {
        $('.sombra', window.parent.document).fadeOut('fast');
        $('.confirm', window.parent.document).fadeOut('fast');
    }
}

function Confirm(info) {

    if (info == "true") {
        ConfirmDelete();
    }
    else {
        $('.confirm', window.parent.document).fadeOut('fast');
        $('.sombra', window.parent.document).fadeOut('fast');
    }

}


function PopAlert(msm) {
    document.getElementById('msm_alert').innerHTML = msm;
    $('.sombra').fadeIn('fast');
    $('.alert').fadeIn('fast');

    switch (msm) {
        case 'Event Created Successfully':
            $('.alert i').attr("class", "icon-drink");
            break;

        case 'Successful registration':
            $('.alert i').attr("class", "icon-Bandera");
            break;

        case 'Updated':
            $('.alert i').attr("class", "icon-Bandera");
            break;

        default:
            $('.alert i').attr("class", "icon-alert");
            break;

    }
}

function CloseAlert() {
    $('.sombra').fadeOut('fast');
    $('.alert').fadeOut('fast');
}

var boxTag = [];

function CheckChange(check) {

    var estado = document.getElementById(check).checked;
    boxTag = document.getElementsByClassName('box-tag');

    switch (check) {

        case 'licores':

            if (estado) {
                localStorage.setItem('spirits', 'Standard');
            }
            else {
                localStorage.setItem('spirits', 'Premium');
            }

            for (var i = 0; i < boxTag.length; i++) {

                if(estado){
                    if (boxTag[i].getAttribute("purple") == "true") {
                        boxTag[i].setAttribute("purple", "disabled");
                    }
                }
                else{
                    if (boxTag[i].getAttribute("purple") == "disabled") {
                        boxTag[i].setAttribute("purple", "true");
                    }
                }
                
            }

            break;

        case 'foods':
            if (estado) {
                $("#table-box").slideDown('fast');
                localStorage.setItem('food', 'Yes');
            }
            else {
                $("#table-box").slideUp('fast');
                localStorage.setItem('food', 'None');
            }
            break;
    }



}

function Time(idTime) {

    switch (idTime) {
        case 'time1':
            var ini = document.getElementById('time1').value.substring(0, 2);
            var final = document.getElementById('time1').value.substring(3);
            var suma = parseInt(ini) + 4;
            document.getElementById('time2').value = suma + ":" + final;
            document.getElementById('fullTime').value = "Event Time: Start" + " " + tConvert(document.getElementById('time1').value) + " End " + " " + tConvert(document.getElementById('time2').value);
            break;

        case 'time2':
            document.getElementById('fullTime').value = "Event Time: Start" + " " + tConvert(document.getElementById('time1').value) + " End " + " " + tConvert(document.getElementById('time2').value);
            break;
    }


}

function DeleteEvent(id) {
    $('.sombra', window.parent.document).fadeIn('fast');
    $('.confirm', window.parent.document).fadeIn('fast');
    localStorage.setItem('id_evento_delete', id);
}

function ConfirmDelete() {
    var obj_idEvent = JSON.stringify(localStorage.getItem('id_evento_delete'));

    $.ajax({
        cache: false,
        method: 'post',
        url: 'php/delete_event.php',
        data: { obj_idEvent: obj_idEvent },
        success: function (res) {
            Confirm('false');
            window.open('master.php', '_self');
        }
    });
}

//Funcion de pago 

function Payment(estado) {

    //valores de estado 'archivado' , ' activo';

    var obj_nameEvent = JSON.stringify(localStorage.getItem("nameEvent"));
    var obj_address1 = JSON.stringify(localStorage.getItem("address1"));
    var obj_address2 = JSON.stringify(localStorage.getItem("address2"));
    var obj_county = JSON.stringify(localStorage.getItem("county"));
    var obj_city = JSON.stringify(localStorage.getItem("city"));
    var obj_zip_code = JSON.stringify(localStorage.getItem("zip_code"));
    var obj_guest = JSON.stringify(localStorage.getItem('guest'));
    var obj_time1 = JSON.stringify(localStorage.getItem('ini_hour'));
    var obj_time2 = JSON.stringify(localStorage.getItem('fin_hour'));
    var obj_dateEvent = JSON.stringify(localStorage.getItem('dateEvent'));
    var obj_hourEvent = JSON.stringify(localStorage.getItem('hourEvent'));
    var obj_estado = JSON.stringify(estado);
    var obj_experience = JSON.stringify(localStorage.getItem('experience'));
    var obj_wines = JSON.stringify(localStorage.getItem('wines'));

    var obj_spirits = JSON.stringify(localStorage.getItem('spirits'));
    var obj_foods = JSON.stringify(localStorage.getItem('food'));

    $.ajax({
        cache: false,
        method: 'post',
        url: 'php/save_event.php',
        data: {
            obj_nameEvent: obj_nameEvent,
            obj_address1: obj_address1,
            obj_address2: obj_address2,
            obj_county: obj_county,
            obj_city: obj_city,
            obj_zip_code: obj_zip_code,
            obj_guest: obj_guest,
            obj_time1: obj_time1,
            obj_time2: obj_time2,
            obj_dateEvent: obj_dateEvent,
            obj_hourEvent: obj_hourEvent,
            obj_estado: obj_estado,
            obj_experience: obj_experience,
            obj_wines: obj_wines,
            obj_spirits: obj_spirits,
            obj_foods: obj_foods
        },
        success: function (res) {

            switch (res) {

                case 'archivado':
                    window.open('create_event_3.php', '_self');
                    break;

                case 'update':
                    window.open('create_event_3.php', '_self');
                    break;

                case 'ok':
                    if (estado == 'activo') {
                        $('.sombra', window.parent.document).fadeIn('fast');
                        $('.pay', window.parent.document).fadeIn('fast');
                    }
                    break;

                case 'no_event':
                    window.parent.PopAlert("We're sorry, we're booked for the date you selected.");
                    break;

                default:
                    window.parent.PopAlert('Error creating event: ' + res);
                    break;

            }
        }
    });

    //window.open('home_user.html','_self');
}
