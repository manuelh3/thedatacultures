<?php

session_start();

include 'conect.php';

$date_event = "";
$full_event = '{"start":"ini","color":"ini"}';
$color_green = "rgb(0,255,117)";
$color_gray = "rgb(132,133,132)";
$color_red = "rgb(235,83,84)";

$estado = "";
$id_event = "";

$date_actual = strtotime(date("Y-m-d",time()));

/*[{"start":"2022-01-09","color":"rgb(132, 133, 132)"}]*/

$sql = "SELECT * FROM eventos WHERE estado='activo'";
$result = mysqli_query($con,$sql);

while($fila = mysqli_fetch_assoc($result)){
    $date_event = $fila['date_event'];
    $estado = $fila['estado'];
    $id_event = $fila['id_evento'];

    if(strtotime($date_event) < $date_actual && $estado == "activo"){
        $full_event .= ',{"start":'.'"'. $date_event .'","color":"'.$color_gray.'","id":"'.$id_event.'"}';
    }
    else if($estado == "activo"){
        $full_event .= ',{"start":'.'"'. $date_event .'","color":"'.$color_green.'","id":"'.$id_event.'"}';
    }
    else{
        $full_event .= ',{"start":'.'"'. $date_event .'","color":"'.$color_red.'","id":"'.$id_event.'"}';
    }

    
}

$json_date = "[".$full_event."]";

echo $json_date;

?>