<!DOCTYPE html>

<html>

<head>
    <link href="css/estilos.css" type="text/css" rel="stylesheet">
    <meta charset="utf-8">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <script src="https://kit.fontawesome.com/0bddffe200.js" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>

    <!--Librerias de FullCalendar-->
    <script src="fullcalendar/lib/moment.min.js"></script>
    <link rel="stylesheet" href="fullcalendar/fullcalendar.min.css">
    <script src="fullcalendar/fullcalendar.min.js"></script>

    <!---->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/funciones.js" type="text/jscript"></script>


</head>

<body>

    <?php

    include 'php/conect.php';

    $sql = "SELECT * FROM eventos INNER JOIN usuarios ON eventos.email_user = usuarios.email WHERE eventos.estado='activo' ORDER BY eventos.date_event DESC";
    $result = mysqli_query($con, $sql);

    $id_event = "";

    $user_f_name = "";
    $user_l_name = "";
    $event_date = "";
    $city = "";
    $address = "";
    $experience = "";

    $html_list = "";
    $cont = 0;

    $icon = "";
    $check_admin = "";
    $color = "";


    while ($fila  = mysqli_fetch_assoc($result)) {
        $id_event = $fila['id_evento'];
        $user_f_name = $fila['f_name'];
        $user_l_name = $fila['l_name'];
        $event_date = $fila['date_event'];
        $city = $fila['city'];
        $address = $fila['address1'];
        $check_admin = $fila['check_admin'];
        $experience = $fila['experience'];

        if ($check_admin == "false") {
            $cont++;
        }


        if ($check_admin == "true") {
            $icon = "icon-Bandera";
            $check_admin = 'checked';
            $color = '#8BC581';
        } else {
            $icon = "icon-Campana";
            $check_admin = '';
            $color = 'red';
        }

        $date_ini = date_create($event_date);
        $date_set = date_format($date_ini, "m/d/Y");

        $html_list .= '<tr>
        <td>' . $user_f_name . ' ' . $user_l_name . '</td>
        <td>' . $date_set . '</td>
        <td>' . $city . '</td>
        <td>' . $address . '</td>
        <td>' . $experience . '</td>
        <td><br><label class="checkbox">
                <input type="checkbox" id="' . $id_event . '" onclick="ChangeCheck(this.id)" ' . $check_admin . '>
                <i class="fas fa-check-square"></i>
            </label></td>
        <td><i class="' . $icon . '" id="' . $id_event . '" onclick="GetEvent(this.id)" style="font-size: 30px; color:' . $color . '; cursor:pointer"></i></td>
    </tr>';
    }

    ?>
    <section class="window">
        <div class="info_event_admin">
            <i class="icon-close" onclick="CloseDateEvent()" style="font-size: 30px; cursor:pointer;float:right; padding:20px"></i>
            <nav>
                <table style="width: 90%; position:absolute; z-index:0">
                    <tr>
                        <td>
                            <h2>Event</h2>
                            <p id="name">Noche Julio</p>
                        </td>
                        <td>
                            <h2>Email</h2>
                            <p id="email">Noche Julio</p>
                        </td>
                        <td>
                            <h2>Food</h2>
                            <p id="food">None</p>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <h2>Address1</h2>
                            <p id="address1">County</p>
                        </td>
                        <td>
                            <h2>Address2</h2>
                            <p id="address2">Noche Julio</p>
                        </td>
                        <td>
                            <h2>Spirits</h2>
                            <p id="spirits">Premium</p>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <h2>County</h2>
                            <p id="county">Noche Julio</p>
                        </td>
                        <td>
                            <h2>City</h2>
                            <p id="city">Noche Julio</p>
                        </td>
                        <td>
                            <h2>Zip Code</h2>
                            <p id="zip_code">Premium</p>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <h2>Date Event</h2>
                            <p id="date">Noche Julio</p>
                        </td>
                        <td>
                            <h2>Time Event</h2>
                            <p id="time">Noche Julio</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2>Guest</h2>
                            <p>$ <span id="guest">3000</span></p>
                        </td>
                        <td>
                            <h2>Wines</h2>
                            <p id="wines">Noche Julio</p>
                        </td>
                    </tr>

                </table>
            </nav>
        </div>
    </section>


    <div class="content home_user">

        <nav class="rigth home_user" style="width: 100%;">

            <section class="programa">
                <br><br><br>
                <section class="listas">
                    <div class="calendario_2">
                        <div id="inside_calendario_2">
                            <table class="table1 title">
                                <thead>
                                    <tr>
                                        <th>USER</th>
                                        <th>EVENT DATE</th>
                                        <th>CITY</th>
                                        <th>ADDRESS</th>
                                        <th>EXPERIENCE</th>
                                        <th>GO</th>
                                        <th>INFO</th>
                                    </tr>
                                </thead>
                            </table>
                            <section class="contenido_listas">
                                <table class="table_listas">
                                    <tbody>
                                        <?php echo $html_list; ?>
                                        <!-- <tr>
                                            <td>4th july celebration</td>
                                            <td>2022-01-27</td>
                                            <td>2022-01-27</td>
                                            <td>FP000586</td>
                                            <td>FP000586</td>
                                            <td><label class="checkbox">
                                                    <input type="checkbox">
                                                    <i class="fas fa-check-square"></i>
                                                </label></td>
                                            <td><i class="icon-Campana" style="font-size: 30px; cursor:pointer"></i></td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </section>
                        </div>
                    </div>
                </section>

            </section>

        </nav>

    </div>

    <script>
        window.onload = function() {

            var contador_pop = <?php echo $cont; ?>;
            if (contador_pop < 1) {
                $('.pop', window.parent.document).fadeOut(0);
            } else {
                if (contador_pop > 10) {
                    $('.pop', window.parent.document).fadeIn(0);
                    $('#indicador_pop1', window.parent.document).text('+');
                } else {
                    $('.pop', window.parent.document).fadeIn(0);
                    $('#indicador_pop1', window.parent.document).text(contador_pop);
                }

            }
            $('.loader', window.parent.document).fadeOut('fast');
        }

        function Next() {
            window.open('create_event.html', '_self');
            $('#drink', window.parent.document).attr('active', 'true');
            $('#home', window.parent.document).attr('active', 'false');
        }

        function ChangeSection(btn) {
            switch (btn) {
                case 'btn_calendar':
                    $('#btn_calendar').attr('state', 'enabled');
                    $('#btn_list').attr('state', '');
                    $('.calendarios').fadeIn(0);
                    $('.listas').fadeOut(0);
                    break;

                case 'btn_list':
                    $('#btn_list').attr('state', 'enabled');
                    $('#btn_calendar').attr('state', '');
                    $('.calendarios').fadeOut(0);
                    $('.listas').fadeIn(0);
                    break;

            }
        }

        function ChangeCheck(id) {

            var check = document.getElementById(id).checked;

            var obj_function = JSON.stringify('check_event');
            var obj_event = JSON.stringify(id);
            var obj_state = JSON.stringify(check.toString());

            $.ajax({
                cache: false,
                method: 'post',
                url: 'php/funciones_php.php',
                data: {
                    obj_function: obj_function,
                    obj_event: obj_event,
                    obj_state: obj_state
                },
                success: function() {
                    parent.location.reload();
                }
            });

        }

        function CloseDateEvent() {
            $('.info_event_admin').fadeOut('fast');
        }

        function GetEvent(evento) {
            var obj_function = JSON.stringify('GetEvent');
            var obj_event = JSON.stringify(evento);
            var obj_state = JSON.stringify('activo');

            $.ajax({
                cache: false,
                method: 'post',
                url: 'php/funciones_php.php',
                data: {
                    obj_function: obj_function,
                    obj_event,
                    obj_event,
                    obj_state: obj_state
                },
                success: function(res) {
                    var datos = JSON.parse(res);
                    document.getElementById('name').innerHTML = datos.name;
                    document.getElementById('email').innerHTML = datos.email;
                    document.getElementById('address1').innerHTML = datos.address1;
                    document.getElementById('address2').innerHTML = datos.address2;
                    document.getElementById('county').innerHTML = datos.county;
                    document.getElementById('city').innerHTML = datos.city;
                    document.getElementById('date').innerHTML = datos.date;
                    document.getElementById('time').innerHTML = datos.time;
                    document.getElementById('wines').innerHTML = datos.wines;
                    document.getElementById('spirits').innerHTML = datos.spirits;
                    document.getElementById('food').innerHTML = datos.food;
                    document.getElementById('zip_code').innerHTML = datos.zip_code;
                    document.getElementById('guest').innerHTML = parseInt(datos.guest).toLocaleString("es-Us") + '.00';
                    $('.info_event_admin').fadeIn('fast');

                }
            });
        }
    </script>

</body>



</html>